/*
 *  $Id: volume_stepline.c 26354 2024-05-21 08:22:32Z yeti-dn $
 *  Copyright (C) 2018 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwythreads.h>
#include <libprocess/brick.h>
#include <libprocess/level.h>
#include <libprocess/arithmetic.h>
#include <libprocess/correct.h>
#include <libprocess/filters.h>
#include <libprocess/linestats.h>
#include <libprocess/stats.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-volume.h>
#include <app/gwyapp.h>
#include "libgwyddion/gwyomp.h"

#define VOLUME_STEPLINE_RUN_MODES (GWY_RUN_IMMEDIATE)

static gboolean module_register   (void);
static void     volume_stepline  (GwyContainer *data,
                                  GwyRunType run);
static void     brick_level       (GwyBrick *brick);
static void     step_line_correct (GwyDataField *dfield);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Performs step line correction"),
    "Yeti <yeti@gwyddion.net>, Luke Somers <lsomers@sas.upenn.edu>, Petr Klapetek <klapetek@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti) & Petr Klapetek & Luke Somers",
    "2023",
};

GWY_MODULE_QUERY2(module_info, volume_stepline)

static gboolean
module_register(void)
{
    gwy_volume_func_register("volume_stepline",
                             (GwyVolumeFunc)&volume_stepline,
                             N_("/_Correct Data/Ste_p Line Correction"),
                             NULL,
                             VOLUME_STEPLINE_RUN_MODES,
                             GWY_MENU_FLAG_VOLUME,
                             N_("Step line correction in all XY planes"));

    return TRUE;
}

static void
volume_stepline(GwyContainer *data, GwyRunType run)
{
    GwyBrick *brick = NULL;
    gint id, newid;

    g_return_if_fail(run & VOLUME_STEPLINE_RUN_MODES);

    gwy_app_data_browser_get_current(GWY_APP_BRICK, &brick,
                                     GWY_APP_BRICK_ID, &id,
                                     0);
    g_return_if_fail(GWY_IS_BRICK(brick));

    brick = gwy_brick_duplicate(brick);
    gwy_app_wait_start(gwy_app_find_window_for_volume(data, id), _("Step line correction..."));
    brick_level(brick);
    gwy_app_wait_finish();
    newid = gwy_app_data_browser_add_brick(brick, NULL, data, TRUE);
    gwy_app_set_brick_title(data, newid, _("Step line corrected"));
    g_object_unref(brick);

    gwy_app_sync_volume_items(data, data, id, newid, FALSE,
                              GWY_DATA_ITEM_GRADIENT,
                              0);

    gwy_app_volume_log_add_volume(data, id, newid);
}

static void
brick_level(GwyBrick *brick)
{
    gint xres = gwy_brick_get_xres(brick);
    gint yres = gwy_brick_get_yres(brick);
    gint zres = gwy_brick_get_zres(brick);

#ifdef _OPENMP
#pragma omp parallel if(gwy_threads_are_enabled()) default(none) \
            shared(brick,xres,yres,zres)
#endif
    {
        GwyDataField *dfield = gwy_data_field_new(xres, yres, xres, yres,
                                                  FALSE);
        gint k;
        gint kfrom = gwy_omp_chunk_start(zres), kto = gwy_omp_chunk_end(zres);
        for (k = kfrom; k < kto; k++) {
            gwy_brick_extract_xy_plane(brick, dfield, k);
            step_line_correct(dfield);
            gwy_brick_set_xy_plane(brick, dfield, k);
        }

        g_object_unref(dfield);
    }
}

static void
calcualte_segment_correction(const gdouble *drow,
                             gdouble *mrow,
                             gint xres,
                             gint len)
{
    const gint min_len = 4;
    gdouble corr;
    gint j;

    if (len >= min_len) {
        corr = 0.0;
        for (j = 0; j < len; j++)
            corr += (drow[j] + drow[2*xres + j])/2.0 - drow[xres + j];
        corr /= len;
        for (j = 0; j < len; j++)
            mrow[j] = (3*corr
                       + (drow[j] + drow[2*xres + j])/2.0 - drow[xres + j])/4.0;
    }
    else {
        for (j = 0; j < len; j++)
            mrow[j] = 0.0;
    }
}

static void
line_correct_step_iter(GwyDataField *dfield,
                       GwyDataField *mask)
{
    const gdouble threshold = 3.0;
    gint xres, yres, i, j, len;
    gdouble u, v, w;
    const gdouble *d, *drow;
    gdouble *m, *mrow;

    yres = gwy_data_field_get_yres(dfield);
    xres = gwy_data_field_get_xres(dfield);
    d = gwy_data_field_get_data_const(dfield);
    m = gwy_data_field_get_data(mask);

    w = 0.0;
    for (i = 0; i < yres-1; i++) {
        drow = d + i*xres;
        for (j = 0; j < xres; j++) {
            v = drow[j + xres] - drow[j];
            w += v*v;
        }
    }
    w = w/(yres-1)/xres;

    for (i = 0; i < yres-2; i++) {
        drow = d + i*xres;
        mrow = m + (i + 1)*xres;

        for (j = 0; j < xres; j++) {
            u = drow[xres + j];
            v = (u - drow[j])*(u - drow[j + 2*xres]);
            if (G_UNLIKELY(v > threshold*w)) {
                if (2*u - drow[j] - drow[j + 2*xres] > 0)
                    mrow[j] = 1.0;
                else
                    mrow[j] = -1.0;
            }
        }

        len = 1;
        for (j = 1; j < xres; j++) {
            if (mrow[j] == mrow[j-1])
                len++;
            else {
                if (mrow[j-1])
                    calcualte_segment_correction(drow + j-len, mrow + j-len,
                                                 xres, len);
                len = 1;
            }
        }
        if (mrow[j-1]) {
            calcualte_segment_correction(drow + j-len, mrow + j-len,
                                         xres, len);
        }
    }

    gwy_data_field_sum_fields(dfield, dfield, mask);
}


static void
step_line_correct(GwyDataField *dfield)
{
    GwyDataField *mask;
    GwyDataLine *shifts;
    gdouble avg;

    avg = gwy_data_field_get_avg(dfield);
    shifts = gwy_data_field_find_row_shifts_trimmed_mean(dfield,
                                                         NULL, GWY_MASK_IGNORE,
                                                         0.5, 0);
    gwy_data_field_subtract_row_shifts(dfield, shifts);
    g_object_unref(shifts);

    mask = gwy_data_field_new_alike(dfield, TRUE);
    line_correct_step_iter(dfield, mask);
    gwy_data_field_clear(mask);
    line_correct_step_iter(dfield, mask);
    g_object_unref(mask);

    gwy_data_field_filter_conservative(dfield, 5);
    gwy_data_field_add(dfield, avg - gwy_data_field_get_avg(dfield));
    gwy_data_field_data_changed(dfield);
}


/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
