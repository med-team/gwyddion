/*
 *  $Id: volume_zposlevel.c 26744 2024-10-18 15:03:29Z yeti-dn $
 *  Copyright (C) 2019-2023 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/arithmetic.h>
#include <libprocess/linestats.h>
#include <libprocess/brick.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-volume.h>
#include <app/gwyapp.h>
#include <app/gwymoduleutils.h>

#define RUN_MODES (GWY_RUN_INTERACTIVE)

enum {
    PREVIEW_SIZE = 360,
    RANGE_MAX = 128,
};

enum {
    PARAM_XPOS,
    PARAM_YPOS,
    PARAM_ZPOS,
    PARAM_RANGE,
    PARAM_SHOW_TYPE,

    INFO_WVALUE,
};

typedef enum {
    SHOW_DATA  = 0,
    SHOW_RESULT = 1,
} ZposlevelShow;

typedef struct {
    GwyParams *params;
    GwyBrick *brick;
    GwyBrick *result;
    GwyDataLine *calibration;
} ModuleArgs;

typedef struct {
    ModuleArgs *args;
    GwyContainer *data;
    GwyDataField *xyplane;
    GtkWidget *dialog;
    GwyGraphModel *gmodel;
    GtkWidget *dataview;
    GwyParamTable *table_value;
    GwyParamTable *table_output;
    GwySelection *image_selection;
    GwySelection *graph_selection;
    GwySIValueFormat *vf;
} ModuleGUI;

static gboolean         module_register        (void);
static GwyParamDef*     define_module_params   (void);
static void             zposlevel              (GwyContainer *data,
                                                GwyRunType run);
static void             execute                (ModuleArgs *args);
static GwyDialogOutcome run_gui                (ModuleArgs *args,
                                                GwyContainer *data,
                                                gint id);
static void             param_changed          (ModuleGUI *gui,
                                                gint id);
static void             preview                (gpointer user_data);
static void             dialog_response_after  (GtkDialog *dialog,
                                                gint response,
                                                ModuleGUI *gui);
static void             point_selection_changed(ModuleGUI *gui,
                                                gint id,
                                                GwySelection *selection);
static void             graph_selection_changed(ModuleGUI *gui,
                                                gint id,
                                                GwySelection *selection);
static void             extract_xyplane        (ModuleGUI *gui);
static void             extract_graph_curve    (ModuleArgs *args,
                                                GwyGraphCurveModel *gcmodel);
static gdouble          get_constant_value     (ModuleArgs *args);
static void             setup_gmodel           (ModuleArgs *args,
                                                GwyGraphModel *gmodel);
static void             sanitise_params        (ModuleArgs *args);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Shifts values in z curves to be zero at defined position."),
    "Petr Klapetek <klapetek@gwyddion.net>",
    "2.2",
    "Petr Klapetek",
    "2019",
};

GWY_MODULE_QUERY2(module_info, volume_zposlevel)

static gboolean
module_register(void)
{
    gwy_volume_func_register("volume_zposlevel",
                             (GwyVolumeFunc)&zposlevel,
                             N_("/_Correct Data/S_hift Value To Zero..."),
                             NULL,
                             RUN_MODES,
                             GWY_MENU_FLAG_VOLUME,
                             N_("Shift value at some z plane to zero"));

    return TRUE;
}

static GwyParamDef*
define_module_params(void)
{
    static const GwyEnum displays[] = {
        { N_("_Data"),   SHOW_DATA,  },
        { N_("_Result"), SHOW_RESULT, },
    };
    static GwyParamDef *paramdef = NULL;

    if (paramdef)
        return paramdef;

    paramdef = gwy_param_def_new();
    gwy_param_def_set_function_name(paramdef, gwy_volume_func_current());
    gwy_param_def_add_int(paramdef, PARAM_XPOS, "xpos", _("_X"), -1, G_MAXINT, -1);
    gwy_param_def_add_int(paramdef, PARAM_YPOS, "ypos", _("_Y"), -1, G_MAXINT, -1);
    gwy_param_def_add_int(paramdef, PARAM_ZPOS, "zpos", _("_Z value"), -1, G_MAXINT, -1);
    gwy_param_def_add_int(paramdef, PARAM_RANGE, "range", _("_Z range"), 1, RANGE_MAX, 1);
    gwy_param_def_add_gwyenum(paramdef, PARAM_SHOW_TYPE, "show_type", gwy_sgettext("verb|_Display"),
                              displays, G_N_ELEMENTS(displays), SHOW_DATA);
    return paramdef;
}

static void
zposlevel(GwyContainer *data, GwyRunType run)
{
    ModuleArgs args;
    GwyDialogOutcome outcome;
    gchar *title;
    gint id, newid;

    g_return_if_fail(run & RUN_MODES);
    g_return_if_fail(g_type_from_name("GwyLayerPoint"));

    gwy_clear(&args, 1);
    gwy_app_data_browser_get_current(GWY_APP_BRICK, &args.brick,
                                     GWY_APP_BRICK_ID, &id,
                                     0);
    g_return_if_fail(GWY_IS_BRICK(args.brick));

    args.calibration = gwy_brick_get_zcalibration(args.brick);
    if (args.calibration && (gwy_brick_get_zres(args.brick) != gwy_data_line_get_res(args.calibration)))
        args.calibration = NULL;
    args.params = gwy_params_new_from_settings(define_module_params());
    sanitise_params(&args);
    args.result = gwy_brick_duplicate(args.brick);

    outcome = run_gui(&args, data, id);
    gwy_params_save_to_settings(args.params);
    if (outcome == GWY_DIALOG_CANCEL)
        goto end;

    if (outcome != GWY_DIALOG_HAVE_RESULT)
        execute(&args);

    newid = gwy_app_data_browser_add_brick(args.result, NULL, data, TRUE);
    title = g_strdup_printf(_("Shifted to zero for z level = %d"), gwy_params_get_int(args.params, PARAM_ZPOS));
    gwy_container_set_string(data, gwy_app_get_brick_title_key_for_id(newid), title);
    gwy_app_sync_volume_items(data, data, id, newid, FALSE,
                              GWY_DATA_ITEM_GRADIENT,
                              0);
    gwy_app_volume_log_add_volume(data, id, newid);

end:
    g_object_unref(args.result);
    g_object_unref(args.params);
}

static GwyDialogOutcome
run_gui(ModuleArgs *args, GwyContainer *data, gint id)
{
    GwyBrick *brick = args->brick;
    gint zres = gwy_brick_get_zres(brick);
    GtkWidget *hbox, *align;
    GwyGraphArea *area;
    GwyGraph *graph;
    GwyParamTable *table;
    GwyDialog *dialog;
    ModuleGUI gui;
    GwyDialogOutcome outcome;
    const guchar *gradient;

    gwy_clear(&gui, 1);
    gui.args = args;

    gui.data = gwy_container_new();
    gui.xyplane = gwy_data_field_new(1, 1, 1.0, 1.0, FALSE);
    extract_xyplane(&gui);
    gwy_container_set_object(gui.data, gwy_app_get_data_key_for_id(0), gui.xyplane);

    if (gwy_container_gis_string(data, gwy_app_get_brick_palette_key_for_id(id), &gradient))
        gwy_container_set_const_string(gui.data, gwy_app_get_data_palette_key_for_id(0), gradient);

    gui.vf = gwy_si_unit_get_format_with_digits(gwy_brick_get_si_unit_w(brick), GWY_SI_UNIT_FORMAT_VFMARKUP,
                                                gwy_brick_get_max(brick) - gwy_brick_get_min(brick), 5, /* 5 digits */
                                                NULL);

    gui.gmodel = gwy_graph_model_new();
    setup_gmodel(args, gui.gmodel);

    gui.dialog = gwy_dialog_new(_("Shift Z to Zero"));
    dialog = GWY_DIALOG(gui.dialog);
    gwy_dialog_add_buttons(dialog, GWY_RESPONSE_UPDATE, GWY_RESPONSE_RESET, GTK_RESPONSE_CANCEL, GTK_RESPONSE_OK, 0);

    hbox = gwy_hbox_new(0);
    gwy_dialog_add_content(dialog, hbox, FALSE, FALSE, 4);

    align = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
    gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 0);

    gui.dataview = gwy_create_preview(gui.data, 0, PREVIEW_SIZE, FALSE);
    gtk_container_add(GTK_CONTAINER(align), gui.dataview);
    gui.image_selection = gwy_create_preview_vector_layer(GWY_DATA_VIEW(gui.dataview), 0, "Point", 1, TRUE);

    graph = GWY_GRAPH(gwy_graph_new(gui.gmodel));
    gwy_graph_enable_user_input(graph, FALSE);
    gtk_widget_set_size_request(GTK_WIDGET(graph), PREVIEW_SIZE, PREVIEW_SIZE);
    gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(graph), TRUE, TRUE, 0);

    area = GWY_GRAPH_AREA(gwy_graph_get_area(graph));
    gwy_graph_area_set_status(area, GWY_GRAPH_STATUS_XLINES);
    gui.graph_selection = gwy_graph_area_get_selection(area, GWY_GRAPH_STATUS_XLINES);
    gwy_selection_set_max_objects(gui.graph_selection, 1);

    hbox = gwy_hbox_new(24);
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), hbox, TRUE, TRUE, 4);

    gui.table_value = table = gwy_param_table_new(args->params);
    gwy_param_table_append_slider(table, PARAM_ZPOS);
    gwy_param_table_slider_restrict_range(table, PARAM_ZPOS, 0, zres-1);
    gwy_param_table_slider_add_alt(table, PARAM_ZPOS);
    if (args->calibration)
        gwy_param_table_alt_set_calibration(table, PARAM_ZPOS, args->calibration);
    else
        gwy_param_table_alt_set_brick_pixel_z(table, PARAM_ZPOS, brick);
    gwy_param_table_append_slider(table, PARAM_RANGE);
    gwy_param_table_set_unitstr(table, PARAM_RANGE, _("px"));
    gwy_param_table_slider_restrict_range(table, PARAM_RANGE, 1, MIN(RANGE_MAX, zres));
    gwy_param_table_append_info(table, INFO_WVALUE, _("Constant value"));
    gwy_param_table_set_unitstr(table, INFO_WVALUE, gui.vf->units);
    gtk_box_pack_start(GTK_BOX(hbox), gwy_param_table_widget(table), FALSE, FALSE, 0);
    gwy_dialog_add_param_table(dialog, table);

    gui.table_output = table = gwy_param_table_new(args->params);
    gwy_param_table_append_radio(table, PARAM_SHOW_TYPE);
    gtk_box_pack_start(GTK_BOX(hbox), gwy_param_table_widget(table), FALSE, FALSE, 0);
    gwy_dialog_add_param_table(dialog, table);

    g_signal_connect_swapped(gui.table_value, "param-changed", G_CALLBACK(param_changed), &gui);
    g_signal_connect_swapped(gui.table_output, "param-changed", G_CALLBACK(param_changed), &gui);
    g_signal_connect_swapped(gui.image_selection, "changed", G_CALLBACK(point_selection_changed), &gui);
    g_signal_connect_swapped(gui.graph_selection, "changed", G_CALLBACK(graph_selection_changed), &gui);
    g_signal_connect_after(dialog, "response", G_CALLBACK(dialog_response_after), &gui);
    gwy_dialog_set_preview_func(dialog, GWY_PREVIEW_UPON_REQUEST, preview, &gui, NULL);

    outcome = gwy_dialog_run(dialog);

    g_object_unref(gui.gmodel);
    g_object_unref(gui.xyplane);
    g_object_unref(gui.data);
    gwy_si_unit_value_format_free(gui.vf);

    return outcome;
}

static void
point_selection_changed(ModuleGUI *gui,
                        G_GNUC_UNUSED gint id,
                        GwySelection *selection)
{
    ModuleArgs *args = gui->args;
    GwyBrick *brick = args->brick;
    gint xres = gwy_brick_get_xres(brick), yres = gwy_brick_get_yres(brick);
    gdouble xy[2];

    if (!gwy_selection_get_object(selection, 0, xy))
        return;

    gwy_params_set_int(args->params, PARAM_XPOS, CLAMP(gwy_brick_rtoi(brick, xy[0]), 0, xres-1));
    gwy_params_set_int(args->params, PARAM_YPOS, CLAMP(gwy_brick_rtoj(brick, xy[1]), 0, yres-1));
    gwy_param_table_param_changed(gui->table_value, PARAM_XPOS);
}

static void
graph_selection_changed(ModuleGUI *gui,
                        G_GNUC_UNUSED gint id,
                        GwySelection *selection)
{
    ModuleArgs *args = gui->args;
    GwyBrick *brick = args->brick;
    gint lev, zres = gwy_brick_get_zres(brick);
    gdouble z;

    /* XXX: When clicking on a new position graph emits two updates, one with old selected line removed and another
     * with the new selection. It is silly. Just ignore updates with no selected line. */
    if (!gwy_selection_get_object(selection, 0, &z))
        return;

    lev = GWY_ROUND(gwy_brick_rtok_cal(brick, z));
    lev = CLAMP(lev, 0, zres-1);
    gwy_param_table_set_int(gui->table_value, PARAM_ZPOS, lev);
}

static void
update_constant_value(ModuleGUI *gui)
{
    ModuleArgs *args = gui->args;
    GwySIValueFormat *vf = gui->vf;
    gdouble const_value = get_constant_value(args);
    gchar *s;

    s = g_strdup_printf("%.*f", vf->precision, const_value/vf->magnitude);
    gwy_param_table_info_set_valuestr(gui->table_value, INFO_WVALUE, s);
    g_free(s);
}

static void
param_changed(ModuleGUI *gui, gint id)
{
    ModuleArgs *args = gui->args;
    GwyParams *params = args->params;
    gdouble z;

    if (id < 0 || id == PARAM_ZPOS) {
        z = gwy_brick_ktor_cal(args->brick, gwy_params_get_int(params, PARAM_ZPOS));
        gwy_selection_set_object(gui->graph_selection, 0, &z);
        /* preview() recalculates the shifts, but the image and plot should always be updated immediately. We do not
         * want instant recalculation – that makes the image always black by definition. */
    }
    if (id < 0 || id == PARAM_ZPOS || id == PARAM_SHOW_TYPE) {
        extract_xyplane(gui);
        gwy_data_field_data_changed(gui->xyplane);
    }
    if (id < 0 || id == PARAM_XPOS || id == PARAM_YPOS)
        extract_graph_curve(args, gwy_graph_model_get_curve(gui->gmodel, 0));
    if (id < 0 || id == PARAM_ZPOS || id == PARAM_XPOS || id == PARAM_YPOS)
        update_constant_value(gui);
    gwy_dialog_invalidate(GWY_DIALOG(gui->dialog));
}

static void
extract_xyplane(ModuleGUI *gui)
{
    ModuleArgs *args = gui->args;
    gint lev = gwy_params_get_int(args->params, PARAM_ZPOS);
    ZposlevelShow show_type = gwy_params_get_enum(args->params, PARAM_SHOW_TYPE);

    gwy_brick_extract_xy_plane(show_type == SHOW_DATA ? args->brick : args->result, gui->xyplane, lev);
}

static void
preview(gpointer user_data)
{
    ModuleGUI *gui = (ModuleGUI*)user_data;
    ModuleArgs *args = gui->args;
    ZposlevelShow show_type = gwy_params_get_enum(args->params, PARAM_SHOW_TYPE);

    /* Keep the result invalidated when showing the original data not running execute()! */
    if (show_type == SHOW_RESULT) {
        execute(args);
        gwy_dialog_have_result(GWY_DIALOG(gui->dialog));
    }
    extract_xyplane(gui);
    gwy_data_field_data_changed(gui->xyplane);
    extract_graph_curve(args, gwy_graph_model_get_curve(gui->gmodel, 0));
}

static void
execute(ModuleArgs *args)
{
    GwyDataField *shifts, *xyplane = NULL;
    GwyBrick *brick = args->result;
    gint zres = gwy_brick_get_zres(brick);
    gint zpos = gwy_params_get_int(args->params, PARAM_ZPOS);
    gint range = gwy_params_get_int(args->params, PARAM_RANGE);
    gint k, zfrom = MAX(zpos - (range - range/2), 0), zto = MIN(zpos + range/2, zres);

    gwy_brick_copy(args->brick, args->result, FALSE);
    shifts = gwy_data_field_new(1, 1, 1.0, 1.0, FALSE);
    /* Unfortunately, gwy_brick_mean_plane() does not do what we need. It cannot limit the range along the axis it is
     * averaging. */
    gwy_brick_extract_xy_plane(brick, shifts, zfrom);
    for (k = zfrom+1; k < zto; k++) {
        if (!xyplane)
            xyplane = gwy_data_field_new_alike(shifts, FALSE);
        gwy_brick_extract_xy_plane(brick, xyplane, zfrom);
        gwy_data_field_sum_fields(shifts, shifts, xyplane);
    }
    gwy_data_field_multiply(shifts, -1.0/(zto - zfrom));
    gwy_brick_add_to_xy_planes(brick, shifts);
    GWY_OBJECT_UNREF(xyplane);
    g_object_unref(shifts);
}

static void
extract_graph_curve(ModuleArgs *args, GwyGraphCurveModel *gcmodel)
{
    ZposlevelShow show_type = gwy_params_get_enum(args->params, PARAM_SHOW_TYPE);
    GwyBrick *brick = show_type == SHOW_DATA ? args->brick : args->result;
    gint zres = gwy_brick_get_zres(brick);
    gint xpos = gwy_params_get_int(args->params, PARAM_XPOS);
    gint ypos = gwy_params_get_int(args->params, PARAM_YPOS);
    GwyDataLine *line, *calibration = args->calibration;

    line = gwy_data_line_new(1, 1.0, FALSE);
    gwy_brick_extract_line(brick, line, xpos, ypos, 0, xpos, ypos, zres, FALSE);
    gwy_data_line_set_offset(line, gwy_brick_get_zoffset(brick));

    if (calibration) {
        gwy_graph_curve_model_set_data(gcmodel,
                                       gwy_data_line_get_data(calibration), gwy_data_line_get_data(line),
                                       gwy_data_line_get_res(line));
        gwy_graph_curve_model_enforce_order(gcmodel);
    }
    else
        gwy_graph_curve_model_set_data_from_dataline(gcmodel, line, 0, 0);

    g_object_unref(line);
}

static gdouble
get_constant_value(ModuleArgs *args)
{
    GwyBrick *brick = args->brick;
    gint zres = gwy_brick_get_zres(brick);
    gint xpos = gwy_params_get_int(args->params, PARAM_XPOS);
    gint ypos = gwy_params_get_int(args->params, PARAM_YPOS);
    gint zpos = gwy_params_get_int(args->params, PARAM_ZPOS);
    gint range = gwy_params_get_int(args->params, PARAM_RANGE);
    gint zfrom = MAX(zpos - (range - range/2), 0), zto = MIN(zpos + range/2, zres);
    GwyDataLine *line;
    gdouble avg;

    if (zfrom+1 >= zto)
        return gwy_brick_get_val(brick, xpos, ypos, zfrom);

    line = gwy_data_line_new(1, 1.0, FALSE);
    gwy_brick_extract_line(brick, line, xpos, ypos, zfrom, xpos, ypos, zto, FALSE);
    avg = gwy_data_line_get_avg(line);
    g_object_unref(line);

    return avg;
}

static void
setup_gmodel(ModuleArgs *args, GwyGraphModel *gmodel)
{
    GwyBrick *brick = args->brick;
    GwyDataLine *calibration = args->calibration;
    GwySIUnit *xunit = (calibration ? gwy_data_line_get_si_unit_y(calibration) : gwy_brick_get_si_unit_z(brick));
    GwyGraphCurveModel *gcmodel;

    g_object_set(gmodel,
                 "label-visible", FALSE,
                 "si-unit-x", xunit,
                 "si-unit-y", gwy_brick_get_si_unit_w(brick),
                 "axis-label-bottom", "z",
                 "axis-label-left", "w",
                 NULL);
    gcmodel = gwy_graph_curve_model_new();
    g_object_set(gcmodel, "mode", GWY_GRAPH_CURVE_LINE, NULL);
    gwy_graph_model_add_curve(gmodel, gcmodel);
    g_object_unref(gcmodel);
}

static void
dialog_response_after(G_GNUC_UNUSED GtkDialog *dialog, gint response, ModuleGUI *gui)
{
    ModuleArgs *args = gui->args;
    GwyBrick *brick = args->brick;

    if (response == GWY_RESPONSE_RESET) {
        gwy_params_set_int(args->params, PARAM_XPOS, gwy_brick_get_xres(brick)/2);
        gwy_params_set_int(args->params, PARAM_YPOS, gwy_brick_get_yres(brick)/2);
        gwy_params_set_int(args->params, PARAM_ZPOS, gwy_brick_get_zres(brick)/2);
    }
}

static inline void
clamp_int_param(GwyParams *params, gint id, gint min, gint max, gint default_value)
{
    gint p = gwy_params_get_int(params, id);

    if (p < min || p > max)
        gwy_params_set_int(params, id, default_value);
}

static void
sanitise_params(ModuleArgs *args)
{
    GwyParams *params = args->params;
    GwyBrick *brick = args->brick;
    gint xres = gwy_brick_get_xres(brick), yres = gwy_brick_get_yres(brick), zres = gwy_brick_get_zres(brick);

    clamp_int_param(params, PARAM_XPOS, 0, xres-1, xres/2);
    clamp_int_param(params, PARAM_YPOS, 0, yres-1, yres/2);
    clamp_int_param(params, PARAM_ZPOS, 0, zres-1, zres/2);
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
