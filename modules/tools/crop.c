/*
 *  $Id: crop.c 26745 2024-10-18 15:17:53Z yeti-dn $
 *  Copyright (C) 2003-2023 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwymodule/gwymodule-tool.h>
#include <libprocess/arithmetic.h>
#include <libgwydgets/gwystock.h>
#include <app/gwyapp.h>

enum {
    PARAM_KEEP_OFFSETS,
    PARAM_ALL,
    PARAM_NEW_CHANNEL,
    PARAM_HOLD_SELECTION,
};

#define GWY_TYPE_TOOL_CROP            (gwy_tool_crop_get_type())
#define GWY_TOOL_CROP(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), GWY_TYPE_TOOL_CROP, GwyToolCrop))
#define GWY_IS_TOOL_CROP(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj), GWY_TYPE_TOOL_CROP))
#define GWY_TOOL_CROP_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), GWY_TYPE_TOOL_CROP, GwyToolCropClass))

typedef struct _GwyToolCrop      GwyToolCrop;
typedef struct _GwyToolCropClass GwyToolCropClass;

struct _GwyToolCrop {
    GwyPlainTool parent_instance;

    GwyParams *params;

    GwyRectSelectionLabels *rlabels;
    GwyParamTable *table;
    gdouble rsel[4];
    gint isel[4];

    /* potential class data */
    GType layer_type_rect;
};

struct _GwyToolCropClass {
    GwyPlainToolClass parent_class;
};

static gboolean     module_register                (void);
static GwyParamDef* define_module_params           (void);
static GType        gwy_tool_crop_get_type         (void)                      G_GNUC_CONST;
static void         gwy_tool_crop_finalize         (GObject *object);
static void         gwy_tool_crop_init_dialog      (GwyToolCrop *tool);
static void         gwy_tool_crop_data_switched    (GwyTool *gwytool,
                                                    GwyDataView *data_view);
static void         gwy_tool_crop_data_changed     (GwyPlainTool *plain_tool);
static void         gwy_tool_crop_response         (GwyTool *tool,
                                                    gint response_id);
static void         gwy_tool_crop_selection_changed(GwyPlainTool *plain_tool,
                                                    gint hint);
static void         gwy_tool_crop_apply            (GwyToolCrop *tool);
static void         param_changed                  (GwyToolCrop *tool,
                                                    gint id);
static void         update_sensitivity             (GwyToolCrop *tool);
static void         update_selected_rectangle      (GwyToolCrop *tool);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Crop tool, crops data to smaller size."),
    "Yeti <yeti@gwyddion.net>",
    "3.2",
    "David Nečas (Yeti) & Petr Klapetek",
    "2003",
};

GWY_MODULE_QUERY2(module_info, crop)

G_DEFINE_TYPE(GwyToolCrop, gwy_tool_crop, GWY_TYPE_PLAIN_TOOL)

static gboolean
module_register(void)
{
    gwy_tool_func_register(GWY_TYPE_TOOL_CROP);

    return TRUE;
}

static GwyParamDef*
define_module_params(void)
{
    static GwyParamDef *paramdef = NULL;

    if (paramdef)
        return paramdef;

    paramdef = gwy_param_def_new();
    gwy_param_def_set_function_name(paramdef, "crop");
    gwy_param_def_add_boolean(paramdef, PARAM_KEEP_OFFSETS, "keep_offsets", _("Keep lateral offsets"), FALSE);
    gwy_param_def_add_boolean(paramdef, PARAM_ALL, "all", _("Crop all compatible images"), FALSE);
    gwy_param_def_add_boolean(paramdef, PARAM_NEW_CHANNEL, "new_channel", _("Create new image"), TRUE);
    gwy_param_def_add_hold_selection(paramdef, PARAM_HOLD_SELECTION, "hold_selection", NULL);

    return paramdef;
}

static void
gwy_tool_crop_class_init(GwyToolCropClass *klass)
{
    GwyPlainToolClass *ptool_class = GWY_PLAIN_TOOL_CLASS(klass);
    GwyToolClass *tool_class = GWY_TOOL_CLASS(klass);
    GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

    gobject_class->finalize = gwy_tool_crop_finalize;

    tool_class->stock_id = GWY_STOCK_CROP;
    tool_class->title = _("Crop");
    tool_class->tooltip = _("Crop data");
    tool_class->prefix = "/module/crop";
    tool_class->data_switched = gwy_tool_crop_data_switched;
    tool_class->response = gwy_tool_crop_response;

    ptool_class->data_changed = gwy_tool_crop_data_changed;
    ptool_class->selection_changed = gwy_tool_crop_selection_changed;
}

static void
gwy_tool_crop_finalize(GObject *object)
{
    GwyToolCrop *tool = GWY_TOOL_CROP(object);

    gwy_params_save_to_settings(tool->params);
    GWY_OBJECT_UNREF(tool->params);

    G_OBJECT_CLASS(gwy_tool_crop_parent_class)->finalize(object);
}

static void
gwy_tool_crop_init(GwyToolCrop *tool)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(tool);

    tool->layer_type_rect = gwy_plain_tool_check_layer_type(plain_tool, "GwyLayerRectangle");
    if (!tool->layer_type_rect)
        return;

    tool->params = gwy_params_new_from_settings(define_module_params());

    plain_tool->lazy_updates = TRUE;
    gwy_plain_tool_connect_selection(plain_tool, tool->layer_type_rect, "rectangle");
    gwy_plain_tool_enable_selection_holding(plain_tool);

    gwy_tool_crop_init_dialog(tool);
}

static void
gwy_tool_crop_rect_updated(GwyToolCrop *tool)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(tool);

    gwy_rect_selection_labels_select(tool->rlabels, plain_tool->selection, plain_tool->data_field);
}

static void
gwy_tool_crop_init_dialog(GwyToolCrop *tool)
{
    GtkDialog *dialog = GTK_DIALOG(GWY_TOOL(tool)->dialog);
    GwyParamTable *table;

    tool->rlabels = gwy_rect_selection_labels_new(TRUE, G_CALLBACK(gwy_tool_crop_rect_updated), tool);
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(dialog)), gwy_rect_selection_labels_get_table(tool->rlabels), FALSE, FALSE, 0);

    table = tool->table = gwy_param_table_new(tool->params);
    gwy_param_table_append_checkbox(table, PARAM_KEEP_OFFSETS);
    gwy_param_table_append_checkbox(table, PARAM_ALL);
    gwy_param_table_append_checkbox(table, PARAM_NEW_CHANNEL);
    gwy_param_table_append_hold_selection(table, PARAM_HOLD_SELECTION);
    gwy_plain_tool_add_param_table(GWY_PLAIN_TOOL(tool), table);
    gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(dialog)), gwy_param_table_widget(table), FALSE, FALSE, 0);

    gwy_plain_tool_add_clear_button(GWY_PLAIN_TOOL(tool));
    gwy_tool_add_hide_button(GWY_TOOL(tool), FALSE);
    gtk_dialog_add_button(dialog, GTK_STOCK_APPLY, GTK_RESPONSE_APPLY);
    gtk_dialog_set_default_response(dialog, GTK_RESPONSE_APPLY);
    gtk_dialog_set_response_sensitive(dialog, GTK_RESPONSE_APPLY, FALSE);
    gwy_help_add_to_tool_dialog(dialog, GWY_TOOL(tool), GWY_HELP_NO_BUTTON);

    update_sensitivity(tool);

    g_signal_connect_swapped(tool->table, "param-changed", G_CALLBACK(param_changed), tool);

    gtk_widget_show_all(gtk_dialog_get_content_area(dialog));
}

static void
gwy_tool_crop_data_switched(GwyTool *gwytool,
                            GwyDataView *data_view)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(gwytool);
    GwyToolCrop *tool = GWY_TOOL_CROP(gwytool);
    gboolean ignore = (data_view == plain_tool->data_view);

    GWY_TOOL_CLASS(gwy_tool_crop_parent_class)->data_switched(gwytool, data_view);

    if (ignore || plain_tool->init_failed)
        return;

    if (data_view) {
        gwy_object_set_or_reset(plain_tool->layer, tool->layer_type_rect,
                                "is-crop", TRUE,
                                "editable", TRUE,
                                "focus", -1,
                                NULL);
        gwy_selection_set_max_objects(plain_tool->selection, 1);
        gwy_plain_tool_hold_selection(plain_tool, gwy_params_get_flags(tool->params, PARAM_HOLD_SELECTION));
    }
}

static void
gwy_tool_crop_data_changed(GwyPlainTool *plain_tool)
{
    update_selected_rectangle(GWY_TOOL_CROP(plain_tool));
}

static void
param_changed(GwyToolCrop *tool, gint id)
{
    if (id < 0 || id == PARAM_ALL)
        update_sensitivity(tool);
}

static void
update_sensitivity(GwyToolCrop *tool)
{
    gwy_param_table_set_sensitive(tool->table, PARAM_NEW_CHANNEL, !gwy_params_get_boolean(tool->params, PARAM_ALL));
}

static void
gwy_tool_crop_response(GwyTool *tool,
                       gint response_id)
{
    GWY_TOOL_CLASS(gwy_tool_crop_parent_class)->response(tool, response_id);

    if (response_id == GTK_RESPONSE_APPLY)
        gwy_tool_crop_apply(GWY_TOOL_CROP(tool));
}

static void
gwy_tool_crop_selection_changed(GwyPlainTool *plain_tool,
                                gint hint)
{
    g_return_if_fail(hint <= 0);
    update_selected_rectangle(GWY_TOOL_CROP(plain_tool));
}

static void
crop_one_field(GwyDataField *field,
               const gint *isel,
               const gdouble *sel,
               gboolean keep_offsets)
{
    gdouble xoff = (keep_offsets ? gwy_data_field_get_xoffset(field) + sel[0] : 0.0);
    gdouble yoff = (keep_offsets ? gwy_data_field_get_yoffset(field) + sel[1] : 0.0);

    gwy_data_field_resize(field, isel[0], isel[1], isel[2]+1, isel[3]+1);
    gwy_data_field_set_xoffset(field, xoff);
    gwy_data_field_set_yoffset(field, yoff);
}

static void
crop_into_new_channel(GwyToolCrop *tool)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(tool);
    GwyContainer *container = plain_tool->container;
    gboolean keep_off = gwy_params_get_boolean(tool->params, PARAM_KEEP_OFFSETS);
    GwyDataField *field = plain_tool->data_field, *mask = plain_tool->mask_field, *show = plain_tool->show_field;
    gint id, oldid = plain_tool->id;
    gint isel[4];
    gdouble rsel[4];

    gwy_assign(isel, tool->isel, 4);
    gwy_assign(rsel, tool->rsel, 4);

    field = gwy_data_field_duplicate(field);
    crop_one_field(field, isel, rsel, keep_off);
    id = gwy_app_data_browser_add_data_field(field, container, TRUE);
    g_object_unref(field);
    gwy_app_sync_data_items(container, container, oldid, id, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_RANGE_TYPE,
                            GWY_DATA_ITEM_MASK_COLOR,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);
    gwy_app_set_data_field_title(container, id, _("Detail"));
    gwy_app_channel_log_add(container, oldid, id, "tool::GwyToolCrop", NULL);

    if (mask) {
        mask = gwy_data_field_duplicate(mask);
        crop_one_field(mask, isel, rsel, keep_off);
        gwy_container_pass_object(container, gwy_app_get_mask_key_for_id(id), mask);
    }

    if (show) {
        show = gwy_data_field_duplicate(show);
        crop_one_field(show, isel, rsel, keep_off);
        gwy_container_pass_object(container, gwy_app_get_show_key_for_id(id), show);
    }
}

static void
crop_in_place_maybe_all(GwyToolCrop *tool)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(tool);
    GwyContainer *container = plain_tool->container;
    GwyDataField *field = plain_tool->data_field, *otherfield;
    gboolean keep_off = gwy_params_get_boolean(tool->params, PARAM_KEEP_OFFSETS);
    gboolean crop_all = gwy_params_get_boolean(tool->params, PARAM_ALL);
    guint i, n;
    gint *ids;
    GArray *undo_quarks;
    gchar *qualname;
    GQuark quark;
    gint isel[4];
    gdouble rsel[4];

    gwy_assign(isel, tool->isel, 4);
    gwy_assign(rsel, tool->rsel, 4);

    if (!crop_all) {
        ids = g_new(gint, 2);
        ids[0] = plain_tool->id;
        ids[1] = -1;
    }
    else
        ids = gwy_app_data_browser_get_data_ids(container);

    undo_quarks = g_array_new(FALSE, FALSE, sizeof(GQuark));
    for (i = n = 0; ids[i] > -1; i++) {
        quark = gwy_app_get_data_key_for_id(ids[i]);
        otherfield = gwy_container_get_object(container, quark);
        if (gwy_data_field_check_compatibility(field, otherfield, GWY_DATA_COMPATIBILITY_ALL))
            continue;

        g_array_append_val(undo_quarks, quark);

        quark = gwy_app_get_mask_key_for_id(ids[i]);
        if (gwy_container_contains(container, quark))
            g_array_append_val(undo_quarks, quark);

        quark = gwy_app_get_show_key_for_id(ids[i]);
        if (gwy_container_contains(container, quark))
            g_array_append_val(undo_quarks, quark);

        ids[n++] = ids[i];
    }
    ids[n] = -1;
    gwy_app_undo_qcheckpointv(container, undo_quarks->len, &g_array_index(undo_quarks, GQuark, 0));
    for (i = 0; i < undo_quarks->len; i++) {
        field = gwy_container_get_object(container, g_array_index(undo_quarks, GQuark, i));
        crop_one_field(field, isel, rsel, keep_off);
        gwy_data_field_data_changed(field);
    }
    qualname = g_strconcat("tool::", G_OBJECT_TYPE_NAME(plain_tool), NULL);
    for (i = 0; ids[i] > -1; i++) {
        gwy_app_data_clear_selections(container, ids[i]);
        gwy_app_channel_log_add(plain_tool->container, ids[i], ids[i], qualname, NULL);
    }
    g_free(qualname);
}

static void
gwy_tool_crop_apply(GwyToolCrop *tool)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(tool);

    g_return_if_fail(plain_tool->id >= 0 && plain_tool->data_field != NULL);

    if (!gwy_selection_get_data(plain_tool->selection, NULL)) {
        g_warning("Apply invoked when no selection is present");
        return;
    }

    gwy_params_save_to_settings(tool->params);   /* Ensure correct parameters in the log. */
    if (!gwy_params_get_boolean(tool->params, PARAM_ALL) && gwy_params_get_boolean(tool->params, PARAM_NEW_CHANNEL))
        crop_into_new_channel(tool);
    else
        crop_in_place_maybe_all(tool);
}

static void
update_selected_rectangle(GwyToolCrop *tool)
{
    GwyPlainTool *plain_tool = GWY_PLAIN_TOOL(tool);
    GwySelection *selection = plain_tool->selection;
    GwyDataField *field = plain_tool->data_field;
    gint n = selection ? gwy_selection_get_data(selection, NULL) : 0;
    gint xres, yres;

    gtk_dialog_set_response_sensitive(GTK_DIALOG(GWY_TOOL(tool)->dialog), GTK_RESPONSE_APPLY, FALSE);
    if (n != 1 || !field) {
        gwy_rect_selection_labels_fill(tool->rlabels, NULL, NULL, tool->rsel, tool->isel);
        return;
    }

    gwy_rect_selection_labels_fill(tool->rlabels, selection, field, tool->rsel, tool->isel);

    /* Make Apply insensitive when the full image is selected, for whatever reason. There is nothting to crop then. */
    xres = gwy_data_field_get_xres(field);
    yres = gwy_data_field_get_yres(field);
    if (tool->isel[2] - tool->isel[0] == xres-1 && tool->isel[3] - tool->isel[1] == yres-1)
        return;

    gtk_dialog_set_response_sensitive(GTK_DIALOG(GWY_TOOL(tool)->dialog), GTK_RESPONSE_APPLY, TRUE);
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
