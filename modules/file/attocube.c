/*
 *  $Id: attocube.c 26163 2024-02-06 16:56:55Z yeti-dn $
 *  Copyright (C) 2009 David Necas (Yeti).
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/* The format is quite similar to SPIP-ASC, however, there are enough
 * differences to warrant another module. */

/**
 * [FILE-MAGIC-FREEDESKTOP]
 * <mime-type type="application/x-attocube-asc">
 *   <comment>Attocube ASCII data</comment>
 *   <magic priority="80">
 *     <match type="string" offset="0" value="# Daisy "/>
 *   </magic>
 *   <glob pattern="*.asc"/>
 *   <glob pattern="*.ASC"/>
 * </mime-type>
 **/

/**
 * [FILE-MAGIC-FILEMAGIC]
 * # Attocube ASCII data.
 * # There can be, unforutnately, various things after Daisy.  But the next
 * # line starts with a date.
 * 0 string \x23\x20Daisy\x20
 * >&0 search/80 \x23\x20
 * >>&0 regex [0-9]{4}-[0-9]{2}-[0-9]{2} Attocube SPM ASCII data
 **/

/**
 * [FILE-MAGIC-USERGUIDE]
 * Attocube Systems ASC
 * .asc
 * Read
 **/

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyutils.h>
#include <libprocess/stats.h>
#include <libgwymodule/gwymodule-file.h>
#include <app/gwymoduleutils-file.h>
#include <app/data-browser.h>

#include "err.h"

// Observed in the wild:
//   Daisy frame view snapshot
//   Daisy saved frame
#define MAGIC "# Daisy "
#define MAGIC_SIZE (sizeof(MAGIC)-1)
#define DATA_MAGIC "# Start of Data:"
#define EXTENSION ".asc"

static gboolean      module_register(void);
static gint          asc_detect     (const GwyFileDetectInfo *fileinfo,
                                     gboolean only_name);
static GwyContainer* asc_load       (const gchar *filename,
                                     GwyRunType mode,
                                     GError **error);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Imports Attocube Systems ASC files."),
    "Yeti <yeti@gwyddion.net>",
    "0.5",
    "David Nečas (Yeti)",
    "2009",
};

GWY_MODULE_QUERY2(module_info, attocube)

static gboolean
module_register(void)
{
    gwy_file_func_register("attocube",
                           N_("Attocube ASCII files (.asc)"),
                           (GwyFileDetectFunc)&asc_detect,
                           (GwyFileLoadFunc)&asc_load,
                           NULL,
                           NULL);

    return TRUE;
}

static gint
asc_detect(const GwyFileDetectInfo *fileinfo,
               gboolean only_name)
{
    if (only_name)
        return g_str_has_suffix(fileinfo->name_lowercase, EXTENSION) ? 10 : 0;

    if (fileinfo->file_size < MAGIC_SIZE || memcmp(fileinfo->head, MAGIC, MAGIC_SIZE) != 0)
        return 0;

    return 100;
}

static GwyContainer*
asc_load(const gchar *filename,
         G_GNUC_UNUSED GwyRunType mode,
         GError **error)
{
    GwyContainer *container = NULL, *meta;
    GwyDataField *dfield = NULL;
    gchar *line, *p, *value, *header, *buffer = NULL;
    GwyTextHeaderParser parser;
    GHashTable *hash = NULL;
    gsize size;
    GError *err = NULL;
    gdouble xreal, yreal, q;
    guint year, month, day, hour, minute, second;
    gint xres, yres, power10;

    if (!g_file_get_contents(filename, &buffer, &size, &err)) {
        err_GET_FILE_CONTENTS(error, &err);
        goto fail;
    }

    p = buffer;
    line = gwy_str_next_line(&p);
    if (!g_str_has_prefix(line, MAGIC)) {
        err_FILE_TYPE(error, "Attocube ASC");
        goto fail;
    }

    line = gwy_str_next_line(&p);
    if (!line || sscanf(line, "# %u-%u-%uT%u:%u:%u", &year, &month, &day, &hour, &minute, &second) != 6) {
        err_FILE_TYPE(error, "Attocube ASC");
        goto fail;
    }

    header = p;
    p = strstr(header, DATA_MAGIC);
    if (!p) {
        err_FILE_TYPE(error, "Attocube ASC");
        goto fail;
    }
    *p = '\0';
    p += strlen(DATA_MAGIC);

    gwy_clear(&parser, 1);
    parser.line_prefix = "#";
    parser.key_value_separator = ":";
    hash = gwy_text_header_parse(header, &parser, NULL, NULL);

    if (!require_keys(hash, error, "x-pixels", "y-pixels", "x-length", "y-length", NULL))
        goto fail;

    xres = atoi(g_hash_table_lookup(hash, "x-pixels"));
    yres = atoi(g_hash_table_lookup(hash, "y-pixels"));
    if (err_DIMENSION(error, xres) || err_DIMENSION(error, yres))
        goto fail;

    xreal = g_ascii_strtod(g_hash_table_lookup(hash, "x-length"), NULL);
    sanitise_real_size(&xreal, "x-length");
    yreal = g_ascii_strtod(g_hash_table_lookup(hash, "y-length"), NULL);
    sanitise_real_size(&yreal, "y-length");

    dfield = gwy_data_field_new(xres, yres, xreal, yreal, FALSE);

    if ((value = g_hash_table_lookup(hash, "x-unit"))) {
        if ((line = g_hash_table_lookup(hash, "y-unit")) && !gwy_strequal(line, value)) {
            g_warning("X and Y units differ, using X");
        }

        gwy_si_unit_set_from_string_parse(gwy_data_field_get_si_unit_xy(dfield), value, &power10);
        q = pow10(power10);
        xreal *= q;
        yreal *= q;
        gwy_data_field_set_xreal(dfield, xreal);
        gwy_data_field_set_yreal(dfield, yreal);
    }
    else
        q = 1.0;

    if ((value = g_hash_table_lookup(hash, "x-offset")))
        gwy_data_field_set_xoffset(dfield, q*g_ascii_strtod(value, NULL));
    if ((value = g_hash_table_lookup(hash, "y-offset")))
        gwy_data_field_set_yoffset(dfield, q*g_ascii_strtod(value, NULL));

    if ((value = g_hash_table_lookup(hash, "z-unit"))) {
        gwy_si_unit_set_from_string_parse(gwy_data_field_get_si_unit_z(dfield), value, &power10);
        q = pow10(power10);
    }
    else
        q = 1.0;

    if (!(gwy_parse_doubles(p, gwy_data_field_get_data(dfield), GWY_PARSE_DOUBLES_FREE_FORM,
                            &yres, &xres, NULL, &err))) {
        err_PARSE_DOUBLES(error, &err);
        GWY_OBJECT_UNREF(dfield);
        goto fail;
    }
    gwy_data_field_multiply(dfield, q);

    container = gwy_container_new();

    gwy_container_pass_object(container, gwy_app_get_data_key_for_id(0), dfield);

    if ((value = g_hash_table_lookup(hash, "display")))
        gwy_container_set_const_string(container, gwy_app_get_data_title_key_for_id(0), value);

    meta = gwy_container_new();
    gwy_container_pass_object(container, gwy_app_get_data_meta_key_for_id(0), meta);

    value = g_strdup_printf("%04u-%02u-%02u %02u:%02u:%02u",
                            year, month, day, hour, minute, second);
    gwy_container_set_string_by_name(meta, "Date", value);

    if ((value = g_hash_table_lookup(hash, "scanspeed")))
        gwy_container_set_const_string_by_name(meta, "Scan Speed", value);

    if ((value = g_hash_table_lookup(hash, "x-offset")))
        gwy_container_set_const_string_by_name(meta, "X Offset", value);

    if ((value = g_hash_table_lookup(hash, "y-offset")))
        gwy_container_set_const_string_by_name(meta, "Y Offset", value);

    gwy_file_channel_import_log_add(container, 0, NULL, filename);

fail:
    g_free(buffer);
    g_hash_table_destroy(hash);

    return container;
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
