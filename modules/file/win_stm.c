/*
 *  $Id: win_stm.c 26493 2024-08-09 15:01:17Z yeti-dn $
 *  Copyright (C) 2014 Jeffrey J. Schwartz.
 *  E-mail: schwartz@physics.ucla.edu
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*
 * This module serves to open WinSTM data files in Gywddion. Currently, multiple data channels are supported with
 * limited meta data import.  No file export is supported; it is assumed that no changes will be saved, or if so then
 * another file format will be used.
 *
 * TODO: Import full meta data list.
 */

/**
 * [FILE-MAGIC-USERGUIDE]
 * WinSTM data
 * .stm
 * Read
 **/

/**
 * [FILE-MAGIC-FREEDESKTOP]
 * <mime-type type="application/x-winstm-spm">
 *   <comment>WinSTM data file</comment>
 *   <magic priority="80">
 *     <match type="string" offset="0" value="WinSTM"/>
 *   </magic>
 * </mime-type>
 **/

#include "config.h"
#include <string.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/stats.h>
#include <libgwymodule/gwymodule-file.h>
#include <app/gwyapp.h>
#include <app/gwymoduleutils-file.h>
#include "err.h"
#include "get.h"

#define EXTENSION ".stm"

#define MAGIC "WinSTM"
#define MAGIC_SIZE (sizeof(MAGIC) - 1)

enum {
    HEADER_SIZE = 1368,
    DATA_SIZE = 768
};

typedef struct {
    gint32 NChan;
    gint32 NSpec;
    gint32 Xres;
    gint32 Yres;
    gdouble Gain;
    gdouble RangeX;
    gdouble RangeY;
    gdouble Bias;
    gdouble Current;
} WinSTM_File;

static gboolean      module_register(void);
static gint          winSTM_detect  (const GwyFileDetectInfo *fileinfo,
                                     gboolean only_name);
static GwyContainer* winSTM_load    (const gchar *filename,
                                     GwyRunType mode,
                                     GError **error);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Imports WinSTM (.stm) files."),
    "Jeffrey J. Schwartz <schwartz@physics.ucla.edu>",
    "0.7",
    "Jeffrey J. Schwartz",
    "April 2014",
};

GWY_MODULE_QUERY2(module_info, win_stm)

static gboolean
module_register(void)
{
    gwy_file_func_register("win_stm",
                           N_("WinSTM files (.stm)"),
                           (GwyFileDetectFunc)&winSTM_detect,
                           (GwyFileLoadFunc)&winSTM_load,
                           NULL,
                           NULL);
    return TRUE;
}

static gint
winSTM_detect(const GwyFileDetectInfo *fileinfo,
              gboolean only_name)
{
    if (only_name)
        return g_str_has_suffix(fileinfo->name_lowercase, EXTENSION) ? 20 : 0;
    if (fileinfo->buffer_len > MAGIC_SIZE && memcmp(fileinfo->head, MAGIC, MAGIC_SIZE) == 0)
        return 100;
    return 0;
}

static GwyContainer*
winSTM_load(const gchar *filename,
            G_GNUC_UNUSED GwyRunType mode,
            GError **error)
{
    WinSTM_File stm;
    GwyContainer *container = NULL;
    GwyDataField *dfield;
    guchar *buffer;
    const guchar *p;
    GError *err = NULL;
    gsize size;
    guint filemin;
    gint id;

    if (!gwy_file_get_contents(filename, &buffer, &size, &err)) {
        err_GET_FILE_CONTENTS(error, &err);
        return NULL;
    }
    filemin = HEADER_SIZE;
    if (size <= filemin) {
        err_TOO_SHORT(error);
        goto end;
    }
    if (memcmp(buffer, MAGIC, MAGIC_SIZE) != 0) {
        err_FILE_TYPE(error, "WinSTM");
        goto end;
    }
    p = buffer + MAGIC_SIZE;
    p += 926;
    stm.NChan = gwy_get_gint32_le(&p);
    stm.NSpec = gwy_get_gint16_le(&p);
    p += 86;
    stm.RangeX = 1e-10 * gwy_get_gdouble_le(&p);
    stm.RangeY = 1e-10 * gwy_get_gdouble_le(&p);
    p += 328;
    filemin = HEADER_SIZE + DATA_SIZE;
    if (size < filemin) {
        err_TOO_SHORT(error);
        goto end;
    }

    container = gwy_container_new();
    for (id = 0; id < stm.NChan; id++) {
        gchar UnitWord[64];
        gchar Title[100];
        GwyContainer *meta;
        const gchar *zunit;
        guint datasize;
        gint power10;

        p += 2;
        get_CHARARRAY0(Title, &p);
        p += 522;
        stm.Gain = gwy_get_gdouble_le(&p);
        get_CHARARRAY0(UnitWord, &p);
        zunit = UnitWord;
        if (strstr(zunit, "ngstroms"))
            zunit = "Å";
        else if (gwy_strequal(zunit, "pAmps"))
            zunit = "pA";
        /* XXX: There was a logic basically treating anything else as ‘V’, but that should be fine as the parser
         * understands things like ‘Volts’ now. */
        stm.Bias = gwy_get_gdouble_le(&p);
        stm.Current = 1e3 * gwy_get_gdouble_le(&p);
        stm.Xres = gwy_get_gint32_le(&p);
        stm.Yres = gwy_get_gint32_le(&p);
        p += 48;
        datasize = sizeof(gint32) * stm.Xres * stm.Yres;
        if (err_SIZE_MISMATCH(error, datasize + (p - buffer), size, FALSE)) {
            GWY_OBJECT_UNREF(container);
            goto end;
        }
        dfield = gwy_data_field_new(stm.Xres, stm.Yres, stm.RangeX, stm.RangeY, FALSE);
        gwy_si_unit_set_from_string(gwy_data_field_get_si_unit_xy(dfield), "m");
        gwy_si_unit_set_from_string_parse(gwy_data_field_get_si_unit_z(dfield), zunit, &power10);
        gwy_convert_raw_data(p, stm.Xres * stm.Yres, 1, GWY_RAW_DATA_SINT32, GWY_BYTE_ORDER_LITTLE_ENDIAN,
                             gwy_data_field_get_data(dfield), pow10(power10) * stm.Gain, 0.0);
        p += datasize;

        gwy_container_pass_object(container, gwy_app_get_data_key_for_id(id), dfield);
        gwy_container_set_string(container,
                                 gwy_app_get_data_title_key_for_id(id),
                                 gwy_convert_to_utf8(Title, -1, "ISO-8859-1"));
        meta = gwy_container_new();
        gwy_container_set_string_by_name(meta, "Bias",
                                         (const guchar *)g_strdup_printf("%.3f V", stm.Bias));
        gwy_container_set_string_by_name(meta, "Current Set Point",
                                         (const guchar *)g_strdup_printf("%.3f pA", stm.Current));
        gwy_container_pass_object(container, gwy_app_get_data_meta_key_for_id(id), meta);
        gwy_file_channel_import_log_add(container, id, NULL, filename);
    }

end:
    gwy_file_abandon_contents(buffer, size, NULL);

    return container;
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
