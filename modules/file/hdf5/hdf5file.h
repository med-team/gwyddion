/*
 *  $Id: hdf5file.h 26429 2024-07-10 15:31:48Z yeti-dn $
 *  Copyright (C) 2024 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#ifndef __GWY_HDF5FILE__
#define __GWY_HDF5FILE__

/* This is a header for the module (as opposed to the HDF5 utils). It should only contain the individual registration
 * functions. */
G_GNUC_INTERNAL void gwyhdf5_register_datxfile(void);
G_GNUC_INTERNAL void gwyhdf5_register_epflfile(void);
G_GNUC_INTERNAL void gwyhdf5_register_ergofile(void);
G_GNUC_INTERNAL void gwyhdf5_register_nhffile(void);
G_GNUC_INTERNAL void gwyhdf5_register_nsidfile(void);
G_GNUC_INTERNAL void gwyhdf5_register_shilpsfile(void);

#endif

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
