/*
 *  $Id: shade.c 25111 2022-10-19 15:02:39Z yeti-dn $
 *  Copyright (C) 2003-2022 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/arithmetic.h>
#include <libprocess/filters.h>
#include <libprocess/stats.h>
#include <libgwydgets/gwystock.h>
#include <libgwydgets/gwyshader.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include "preview.h"

#define RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

enum {
    PARAM_THETA,
    PARAM_PHI,
    PARAM_MIX,
    PARAM_DO_MIX,
};

typedef struct {
    GwyParams *params;
    GwyDataField *field;
    GwyDataField *result;
} ModuleArgs;

typedef struct {
    ModuleArgs *args;
    GtkWidget *dialog;
    GwyParamTable *table;
    GtkWidget *shader;
    GwyContainer *data;
} ModuleGUI;

static gboolean         module_register     (void);
static GwyParamDef*     define_module_params(void);
static void             execute             (ModuleArgs *args);
static GwyDialogOutcome run_gui             (ModuleArgs *args,
                                             GwyContainer *data,
                                             gint id);
static void             param_changed       (ModuleGUI *gui,
                                             gint id);
static void             preview             (gpointer user_data);
static void             shade               (GwyContainer *data,
                                             GwyRunType runtype);
static void             shade_changed       (ModuleGUI *gui,
                                             GwyShader *shader);
static void             mix_with_plane      (GwyDataField *shaded,
                                             GwyDataField *plane,
                                             gdouble mixfraction);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Creates a shaded presentation of data."),
    "Petr Klapetek <klapetek@gwyddion.net>",
    "3.0",
    "David Nečas (Yeti) & Petr Klapetek",
    "2003",
};

GWY_MODULE_QUERY2(module_info, shade)

static gboolean
module_register(void)
{
    gwy_process_func_register("shade",
                              (GwyProcessFunc)&shade,
                              N_("/_Presentation/_Shading..."),
                              GWY_STOCK_SHADER,
                              RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Shade data"));

    return TRUE;
}

static GwyParamDef*
define_module_params(void)
{
    static GwyParamDef *paramdef = NULL;

    if (paramdef)
        return paramdef;

    paramdef = gwy_param_def_new();
    gwy_param_def_set_function_name(paramdef, gwy_process_func_current());
    gwy_param_def_add_angle(paramdef, PARAM_THETA, "theta", _("θ"), TRUE, 4, 0.0);
    gwy_param_def_add_angle(paramdef, PARAM_PHI, "phi", _("φ"), TRUE, 1, 0.0);
    gwy_param_def_add_percentage(paramdef, PARAM_MIX, "mix", _("_Mix"), 0.0);
    gwy_param_def_add_boolean(paramdef, PARAM_DO_MIX, "do_mix", _("_Mix"), FALSE);
    return paramdef;
}

static void
shade(GwyContainer *data, GwyRunType runtype)
{
    GwyDialogOutcome outcome = GWY_DIALOG_PROCEED;
    ModuleArgs args;
    GQuark squark;
    gint id;

    g_return_if_fail(runtype & RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &args.field,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     GWY_APP_SHOW_FIELD_KEY, &squark,
                                     0);
    g_return_if_fail(args.field && squark);

    args.params = gwy_params_new_from_settings(define_module_params());
    args.result = gwy_data_field_new_alike(args.field, TRUE);
    gwy_si_unit_set_from_string(gwy_data_field_get_si_unit_z(args.result), NULL);

    if (runtype == GWY_RUN_INTERACTIVE) {
        outcome = run_gui(&args, data, id);
        gwy_params_save_to_settings(args.params);
        if (outcome == GWY_DIALOG_CANCEL)
            goto end;
    }
    if (outcome != GWY_DIALOG_HAVE_RESULT)
        execute(&args);

    gwy_app_undo_qcheckpointv(data, 1, &squark);

    gwy_container_set_object(data, squark, args.result);
    gwy_app_channel_log_add_proc(data, id, id);

end:
    g_object_unref(args.result);
    g_object_unref(args.params);
}

static GwyDialogOutcome
run_gui(ModuleArgs *args, GwyContainer *data, gint id)
{
    GwyDialogOutcome outcome;
    GtkWidget *hbox, *vbox, *align, *dataview;
    GwyParamTable *table;
    const guchar *pal = NULL;
    ModuleGUI gui;
    GwyDialog *dialog;

    gui.args = args;
    gui.data = gwy_container_new();

    gwy_container_set_object(gui.data, gwy_app_get_data_key_for_id(0), args->result);
    gwy_container_gis_string(data, gwy_app_get_data_palette_key_for_id(id), &pal);
    gwy_app_sync_data_items(data, gui.data, id, 0, FALSE,
                            GWY_DATA_ITEM_PALETTE,
                            GWY_DATA_ITEM_RANGE,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);

    gui.dialog = gwy_dialog_new(_("Shading"));
    dialog = GWY_DIALOG(gui.dialog);
    gwy_dialog_add_buttons(dialog, GWY_RESPONSE_RESET, GTK_RESPONSE_CANCEL, GTK_RESPONSE_OK, 0);

    dataview = gwy_create_preview(gui.data, 0, PREVIEW_SIZE, TRUE);
    hbox = gwy_create_dialog_preview_hbox(GTK_DIALOG(dialog), GWY_DATA_VIEW(dataview), FALSE);

    vbox = gwy_vbox_new(0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);

    align = gtk_alignment_new(0.5, 0.5, 1.0, 1.0);
    gtk_alignment_set_padding(GTK_ALIGNMENT(align), 4, 4, 4, 4);
    gtk_box_pack_start(GTK_BOX(vbox), align, FALSE, TRUE, 0);
    gui.shader = gwy_shader_new(pal);
    gwy_shader_set_angle(GWY_SHADER(gui.shader),
                         gwy_params_get_double(args->params, PARAM_THETA),
                         gwy_params_get_double(args->params, PARAM_PHI));
    gtk_widget_set_size_request(gui.shader, 120, 120);
    gtk_container_add(GTK_CONTAINER(align), gui.shader);

    table = gui.table = gwy_param_table_new(args->params);
    gwy_param_table_append_slider(table, PARAM_THETA);
    gwy_param_table_append_slider(table, PARAM_PHI);
    gwy_param_table_append_slider(table, PARAM_MIX);
    gwy_param_table_add_enabler(table, PARAM_DO_MIX, PARAM_MIX);

    gwy_dialog_add_param_table(dialog, table);
    gtk_box_pack_start(GTK_BOX(vbox), gwy_param_table_widget(table), TRUE, TRUE, 0);

    g_signal_connect_swapped(gui.shader, "angle_changed", G_CALLBACK(shade_changed), &gui);
    g_signal_connect_swapped(table, "param-changed", G_CALLBACK(param_changed), &gui);
    gwy_dialog_set_preview_func(dialog, GWY_PREVIEW_IMMEDIATE, preview, &gui, NULL);

    outcome = gwy_dialog_run(dialog);

    g_object_unref(gui.data);

    return outcome;
}

static void
shade_changed(ModuleGUI *gui, GwyShader *shader)
{
    GwyParamTable *table = gui->table;

    gwy_param_table_set_double(table, PARAM_THETA, fmin(gwy_shader_get_theta(shader), 0.5*G_PI*(1.0 - 1e-15)));
    gwy_param_table_set_double(table, PARAM_PHI, gwy_shader_get_phi(shader));
}

static void
param_changed(ModuleGUI *gui, gint id)
{
    GwyParams *params = gui->args->params;
    /* Read the values first. Do not the one-level recursive shader's update mess up our ϑ and φ. */
    gdouble theta = gwy_params_get_double(params, PARAM_THETA);
    gdouble phi = gwy_params_get_double(params, PARAM_PHI);

    if (id < 0 || id == PARAM_THETA)
        gwy_shader_set_theta(GWY_SHADER(gui->shader), theta);
    if (id < 0 || id == PARAM_PHI)
        gwy_shader_set_phi(GWY_SHADER(gui->shader), phi);

    gwy_dialog_invalidate(GWY_DIALOG(gui->dialog));
}

static void
preview(gpointer user_data)
{
    ModuleGUI *gui = (ModuleGUI*)user_data;

    execute(gui->args);
    gwy_data_field_data_changed(gui->args->result);
}

static void
execute(ModuleArgs *args)
{
    GwyParams *params = args->params;
    GwyDataField *field = args->field, *result = args->result;

    gwy_data_field_shade(field, result,
                         gwy_params_get_double(params, PARAM_THETA), gwy_params_get_double(params, PARAM_PHI));
    if (gwy_params_get_boolean(params, PARAM_DO_MIX))
        mix_with_plane(result, field, gwy_params_get_double(params, PARAM_MIX));
    gwy_data_field_normalize(result);
}

static void
mix_with_plane(GwyDataField *shaded,
               GwyDataField *plane,
               gdouble mixfraction)
{
    gdouble plane_min, plane_max, shade_min, shade_max, plane_range, shade_range, q;

    gwy_data_field_get_min_max(plane, &plane_min, &plane_max);
    gwy_data_field_get_min_max(shaded, &shade_min, &shade_max);
    plane_range = plane_max - plane_min;
    shade_range = shade_max - shade_min;

    q = (plane_range > 0.0 ? mixfraction*shade_range/plane_range : 1.0);
    gwy_data_field_linear_combination(shaded, 1.0 - mixfraction, shaded, q, plane, 0.0);
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
