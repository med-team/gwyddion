/*
 *  $Id: xyz_fft.c 26755 2024-10-23 12:22:00Z yeti-dn $
 *  Copyright (C) 2016-2023 David Necas (Yeti).
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/surface.h>
#include <libprocess/gwyprocess.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-xyz.h>
#include <app/gwyapp.h>
#include <app/gwymoduleutils.h>

#define RUN_MODES (GWY_RUN_INTERACTIVE)

enum {
    PREVIEW_SIZE = 360,
    MAXORDER = 10,
    MAXN = 10000
};

enum {
    RESPONSE_APPLY = 5,
};

enum {
    PARAM_SUPPRESS,
    PARAM_OUTPUT,
    PARAM_ZOOM,
    PARAM_LOGSCALE,
    PARAM_DIR,
};

typedef enum {
    SUPPRESS_NULL         = 0,
    SUPPRESS_NEIGBOURHOOD = 1
} GwyFFTFilt1DSuppressType;

typedef enum {
    OUTPUT_MARKED   = 0,
    OUTPUT_UNMARKED = 1
} GwyFFTFilt1DOutputType;

typedef enum {
    DIR_X  = 0,
    DIR_Y  = 1,
    DIR_Z  = 2,
} FFTDirType;

typedef enum {
    ZOOM_100 = 0,
    ZOOM_20  = 1,
    ZOOM_1   = 2,
} FFTZoomType;


typedef struct {
    GwyParams *params;
    GwySurface *surface;
    GwySurface *result;
    GwySelection *graph_selection;
    GwyDataLine *fftinput;
} ModuleArgs;

typedef struct {
    ModuleArgs *args;
    GtkWidget *dialog;
    GwyParamTable *table_options;
    GwyParamTable *table_axis;
    GwyParamTable *table_view;
    GwyContainer *data;
    GwyGraphModel *gmodel;
    GwyDataField *dfield;
    GwyDataLine *spectrum;
} ModuleGUI;

static gboolean         module_register     (void);
static GwyParamDef*     define_module_params(void);
static gboolean         execute             (ModuleArgs *args,
                                             GtkWindow *wait_window);
static GwyDialogOutcome run_gui             (ModuleArgs *args,
                                             GwyContainer *data,
                                             gint id);
static void             preview             (gpointer user_data);
static void             dialog_response     (GwyDialog *dialog,
                                             gint response,
                                             ModuleGUI *gui);
static void             param_changed       (ModuleGUI *gui,
                                             gint id);
static void             calculate_fft_curve (ModuleGUI *gui);
static void             update_fft_curve    (ModuleGUI *gui);
static void             xyz_fft             (GwyContainer *data,
                                             GwyRunType runtype);
static void             calculate_weights   (GwyDataLine *weights,
                                             GwyDataLine *modulus,
                                             ModuleArgs *args);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("XYZ data 1D FFT correction."),
    "Petr Klapetek <klapetek@gwyddion.net>",
    "1.0",
    "Petr Klapetek",
    "2023",
};

GWY_MODULE_QUERY2(module_info, xyz_fft)

static gboolean
module_register(void)
{
    gwy_xyz_func_register("xyz_fft",
                          (GwyXYZFunc)&xyz_fft,
                          N_("/_FFT Filtering..."),
                          NULL,
                          RUN_MODES,
                          GWY_MENU_FLAG_XYZ,
                          N_("Filter data using FFT"));

    return TRUE;
}

static GwyParamDef*
define_module_params(void)
{
    static const GwyEnum outputs[] = {
        { N_("Marked"),    OUTPUT_MARKED,    },
        { N_("Unmarked"),  OUTPUT_UNMARKED,  },
    };
    static const GwyEnum suppresses[] = {
        { N_("Null"),      SUPPRESS_NULL,         },
        { N_("Suppress"),  SUPPRESS_NEIGBOURHOOD, },
    };
    static const GwyEnum zooms[] = {
        { N_("100 %"),  ZOOM_100, },
        { N_("20 %"),   ZOOM_20,  },
        { N_("1 %"),    ZOOM_1,   },
    };
    static const GwyEnum dirs[] = {
        { N_("X"),    DIR_X,  },
        { N_("Y"),    DIR_Y,  },
        { N_("Z"),    DIR_Z,  },
    };
    static GwyParamDef *paramdef = NULL;

    if (paramdef)
        return paramdef;

    paramdef = gwy_param_def_new();
    gwy_param_def_set_function_name(paramdef, gwy_xyz_func_current());
    gwy_param_def_add_gwyenum(paramdef, PARAM_SUPPRESS, "suppress", _("_Suppress type"),
                              suppresses, G_N_ELEMENTS(suppresses), SUPPRESS_NEIGBOURHOOD);
    gwy_param_def_add_gwyenum(paramdef, PARAM_OUTPUT, "output", _("_Filter type"),
                              outputs, G_N_ELEMENTS(outputs), OUTPUT_UNMARKED);
    gwy_param_def_add_gwyenum(paramdef, PARAM_ZOOM, "zoom", _("_Zoom"),
                              zooms, G_N_ELEMENTS(zooms), ZOOM_20);
    gwy_param_def_add_gwyenum(paramdef, PARAM_DIR, "dir", _("_Axis"),
                              dirs, G_N_ELEMENTS(dirs), DIR_Z);
    gwy_param_def_add_boolean(paramdef, PARAM_LOGSCALE, "logscale", _("_Logarithmic scale"), TRUE);

    return paramdef;
}

static void
xyz_fft(GwyContainer *data, GwyRunType runtype)
{
    GwyDialogOutcome outcome = GWY_DIALOG_PROCEED;
    ModuleArgs args;
    gint id, newid;
    const guchar *gradient;

    g_return_if_fail(runtype & RUN_MODES);

    gwy_clear(&args, 1);
    gwy_app_data_browser_get_current(GWY_APP_SURFACE, &args.surface,
                                     GWY_APP_SURFACE_ID, &id,
                                     0);
    g_return_if_fail(GWY_IS_SURFACE(args.surface));

    args.params = gwy_params_new_from_settings(define_module_params());

    if (runtype == GWY_RUN_INTERACTIVE) {
        outcome = run_gui(&args, data, id);
        gwy_params_save_to_settings(args.params);
        if (outcome == GWY_DIALOG_CANCEL)
            goto end;
    }

    newid = gwy_app_data_browser_add_surface(args.result, data, TRUE);
    gwy_app_set_surface_title(data, newid, _("FFT corrected"));
    if (gwy_container_gis_string(data, gwy_app_get_surface_palette_key_for_id(id), &gradient))
        gwy_container_set_const_string(data, gwy_app_get_surface_palette_key_for_id(newid), gradient);

    g_object_unref(args.result);

end:
    g_object_unref(args.params);

    if (args.fftinput)
        g_object_unref(args.fftinput);
}

static GwyDialogOutcome
run_gui(ModuleArgs *args, GwyContainer *data, gint id)
{
    GwyDialog *dialog;
    ModuleGUI gui;
    GwyParamTable *table;
    gint n;
    GtkWidget *graph, *area, *hbox, *dataview;
    GwyGraphCurveModel *gcmodel;
    const guchar *gradient;
    GwyDialogOutcome outcome;

    gui.dialog = gwy_dialog_new(_("FFT filtering"));
    dialog = GWY_DIALOG(gui.dialog);
    gwy_dialog_add_buttons(dialog, GWY_RESPONSE_RESET, GWY_RESPONSE_UPDATE, GTK_RESPONSE_CANCEL, GTK_RESPONSE_OK, 0);

    gui.args = args;
    gui.data = gwy_container_new();
    gui.dfield = gwy_data_field_new(10, 10, 10, 10, FALSE);
    n = gwy_surface_get_npoints(args->surface);
    args->fftinput = gwy_data_line_new(n, n, FALSE);
    gui.spectrum = gwy_data_line_new(MAXN, n/2 + 1, FALSE);

    gwy_preview_surface_to_datafield(args->surface, gui.dfield, PREVIEW_SIZE, PREVIEW_SIZE, GWY_PREVIEW_SURFACE_FILL);
    gwy_container_set_object(gui.data, gwy_app_get_data_key_for_id(0), gui.dfield);

    if (gwy_container_gis_string(data, gwy_app_get_surface_palette_key_for_id(id), &gradient))
       gwy_container_set_const_string(gui.data, gwy_app_get_data_palette_key_for_id(0), gradient);

    dataview = gwy_create_preview(gui.data, 0, PREVIEW_SIZE, FALSE);
    hbox = gwy_create_dialog_preview_hbox(GTK_DIALOG(dialog), GWY_DATA_VIEW(dataview), FALSE);

    gui.gmodel = gwy_graph_model_new();
    g_object_set(gui.gmodel, "y-logarithmic", TRUE, NULL);
    gcmodel = gwy_graph_curve_model_new();
    g_object_set(gcmodel, "mode", GWY_GRAPH_CURVE_LINE, "description", "FFT data", NULL);
    gwy_graph_model_add_curve(gui.gmodel, gcmodel);
    g_object_unref(gcmodel);

    graph = gwy_graph_new(gui.gmodel);
    gwy_graph_enable_user_input(GWY_GRAPH(graph), FALSE);
    gtk_widget_set_size_request(graph, PREVIEW_SIZE, PREVIEW_SIZE);
    gtk_box_pack_start(GTK_BOX(hbox), graph, TRUE, TRUE, 0);

    area = gwy_graph_get_area(GWY_GRAPH(graph));
    gwy_graph_area_set_status(GWY_GRAPH_AREA(area), GWY_GRAPH_STATUS_XSEL);
    args->graph_selection = gwy_graph_area_get_selection(GWY_GRAPH_AREA(area), GWY_GRAPH_STATUS_XSEL);
    gwy_selection_set_max_objects(args->graph_selection, 20);

    hbox = gwy_hbox_new(10);
    gwy_dialog_add_content(dialog, hbox, FALSE, FALSE, 4);

    gui.table_axis = table = gwy_param_table_new(args->params);
    gwy_param_table_append_radio(table, PARAM_DIR);

    gwy_dialog_add_param_table(dialog, table);
    gtk_box_pack_start(GTK_BOX(hbox), gwy_param_table_widget(table), FALSE, FALSE, 0);

    gui.table_options = table = gwy_param_table_new(args->params);
    gwy_param_table_append_radio(table, PARAM_SUPPRESS);
    gwy_param_table_append_radio(table, PARAM_OUTPUT);

    gwy_dialog_add_param_table(dialog, table);
    gtk_box_pack_start(GTK_BOX(hbox), gwy_param_table_widget(table), FALSE, FALSE, 0);

    gui.table_view = table = gwy_param_table_new(args->params);
    gwy_param_table_append_radio(table, PARAM_ZOOM);
    gwy_param_table_append_checkbox(table, PARAM_LOGSCALE);

    gwy_dialog_add_param_table(dialog, table);
    gtk_box_pack_start(GTK_BOX(hbox), gwy_param_table_widget(table), FALSE, FALSE, 0);

    g_signal_connect_swapped(gui.table_options, "param-changed", G_CALLBACK(param_changed), &gui);
    g_signal_connect_swapped(gui.table_axis, "param-changed", G_CALLBACK(param_changed), &gui);
    g_signal_connect_swapped(gui.table_view, "param-changed", G_CALLBACK(param_changed), &gui);
    g_signal_connect_after(dialog, "response", G_CALLBACK(dialog_response), &gui);
    gwy_dialog_set_preview_func(dialog, GWY_PREVIEW_UPON_REQUEST, preview, &gui, NULL);

    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK, FALSE);

    outcome = gwy_dialog_run(dialog);

    g_object_unref(gui.dfield);
    g_object_unref(gui.data);

    return outcome;
}

static void
preview(gpointer user_data)
{
    ModuleGUI *gui = (ModuleGUI*)user_data;

    if (!execute(gui->args, GTK_WINDOW(gui->dialog)))
        return;

    gwy_preview_surface_to_datafield(gui->args->result, gui->dfield, PREVIEW_SIZE, PREVIEW_SIZE,
                                     GWY_PREVIEW_SURFACE_FILL);

    gwy_data_field_data_changed(gui->dfield);
    gwy_dialog_have_result(GWY_DIALOG(gui->dialog));
    gtk_dialog_set_response_sensitive(GTK_DIALOG(gui->dialog), GTK_RESPONSE_OK, TRUE);
}

static void
dialog_response(GwyDialog *dialog, gint response, ModuleGUI *gui)
{
    if (response == GWY_RESPONSE_RESET) {
        gwy_selection_clear(gui->args->graph_selection);
        gwy_preview_surface_to_datafield(gui->args->surface, gui->dfield, PREVIEW_SIZE, PREVIEW_SIZE,
                                         GWY_PREVIEW_SURFACE_FILL);

        gwy_data_field_data_changed(gui->dfield);
        gwy_dialog_invalidate(GWY_DIALOG(dialog));
        gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK, FALSE);
    }
}

static void
param_changed(ModuleGUI *gui, gint id)
{
    if (id < 0 || id == PARAM_ZOOM) {
        update_fft_curve(gui);
    }
    if (id < 0 || id == PARAM_DIR) {
        calculate_fft_curve(gui);
        update_fft_curve(gui);
    }
    if (id < 0 || id == PARAM_LOGSCALE) {
        g_object_set(gui->gmodel, "y-logarithmic",
                     gwy_params_get_boolean(gui->args->params, PARAM_LOGSCALE),
                     NULL);
    }
}

static void
calculate_fft_curve(ModuleGUI *gui)
{
    GwySurface *surface = gui->args->surface;
    const GwyXYZ *xyz;
    GwyDataLine *rfft, *ifft;
    gdouble *linedata, *spectrumdata, *realdata, *imagdata, avg;
    gint i, k, m, n;
    gint linen = gwy_data_line_get_res(gui->spectrum);
    gint avn;
    FFTDirType dir = gwy_params_get_enum(gui->args->params, PARAM_DIR);

    gwy_app_wait_start(GTK_WINDOW(gui->dialog), _("Calculating spectrum..."));

    xyz = gwy_surface_get_data_const(surface);
    n = gwy_surface_get_npoints(surface);

    rfft = gwy_data_line_new(n, n, FALSE);
    ifft = gwy_data_line_new(n, n, FALSE);
    linedata = gwy_data_line_get_data(gui->args->fftinput);

    avg = 0;
    if (dir == DIR_X) {
       for (k = 0; k < n; k++) {
          linedata[k] = xyz[k].x;
          avg += xyz[k].x;
       }
    }
    else if (dir == DIR_Y) {
       for (k = 0; k < n; k++) {
          linedata[k] = xyz[k].y;
          avg += xyz[k].y;
       }
    }
    else {
       for (k = 0; k < n; k++) {
          linedata[k] = xyz[k].z;
          avg += xyz[k].z;
       }
    }
    avg /= n;
    gwy_data_line_add(gui->args->fftinput, -avg);

    gwy_data_line_fft_raw(gui->args->fftinput, NULL, rfft, ifft, GWY_TRANSFORM_DIRECTION_FORWARD);
    realdata = gwy_data_line_get_data(rfft);
    imagdata = gwy_data_line_get_data(ifft);

    spectrumdata = gwy_data_line_get_data(gui->spectrum);
    avn = n/linen/2;

    for (k = 0; k < linen; k++) {
        avg = 0;
        for (m = 0; m < avn; m++) {
            i = k*avn + m;
            avg += sqrt(realdata[i]*realdata[i] + imagdata[i]*imagdata[i]);
        }
        spectrumdata[k] = avg/avn;
    }
    gwy_data_line_data_changed(gui->spectrum);

    gwy_app_wait_finish();
    g_object_unref(rfft);
    g_object_unref(ifft);
}

static void
update_fft_curve(ModuleGUI *gui)
{
    gint n = gwy_data_line_get_res(gui->spectrum);
    FFTZoomType zoom = gwy_params_get_enum(gui->args->params, PARAM_ZOOM);
    GwyGraphCurveModel *gcmodel = gwy_graph_model_get_curve(gui->gmodel, 0);

    if (zoom == ZOOM_20)
        n = n/5;
    else if (zoom == ZOOM_1)
        n = n/100;

    gwy_graph_curve_model_set_data_from_dataline(gcmodel, gui->spectrum, 0, n);
}

static gboolean
execute(ModuleArgs *args, GtkWindow *wait_window)
{
    GwySurface *surface = args->surface;
    GwyDataLine *rfft = NULL, *ifft = NULL, *rresult = NULL, *iresult = NULL, *weights = NULL, *fullspectrum = NULL;
    GwySurface *result;
    const GwyXYZ *xyz;
    GwyXYZ *xyz_result;
    gdouble *linedata, *realdata, *imagdata, *rresultdata, *weightdata, *fsdata;
    gint k, n;
    FFTDirType dir = gwy_params_get_enum(args->params, PARAM_DIR);
    gboolean ok = FALSE;

    GWY_OBJECT_UNREF(args->result);

    xyz = gwy_surface_get_data_const(surface);
    n = gwy_surface_get_npoints(surface);

    args->result = result = gwy_surface_new_sized(n);
    xyz_result = gwy_surface_get_data(result);
    gwy_surface_copy_units(surface, result);

    gwy_app_wait_start(wait_window, _("Filtering..."));

    rfft = gwy_data_line_new(n, n, FALSE);
    ifft = gwy_data_line_new(n, n, FALSE);
    rresult = gwy_data_line_new(n, n, FALSE);
    iresult = gwy_data_line_new(n, n, FALSE);
    weights = gwy_data_line_new(n/2 + 1, n/2 + 1, TRUE);
    fullspectrum = gwy_data_line_new(n/2 + 1, n/2 + 1, FALSE);
    linedata = gwy_data_line_get_data(args->fftinput);

    if (dir == DIR_X) {
       for (k = 0; k < n; k++) {
          linedata[k] = xyz[k].x;
       }
    }
    else if (dir == DIR_Y) {
       for (k = 0; k < n; k++) {
          linedata[k] = xyz[k].y;
       }
    }
    else {
       for (k = 0; k < n; k++) {
          linedata[k] = xyz[k].z;
       }
    }

    gwy_data_line_fft_raw(args->fftinput, NULL, rfft, ifft, GWY_TRANSFORM_DIRECTION_FORWARD);

    if (!gwy_app_wait_set_fraction(0.4))
        goto end;

    fsdata = gwy_data_line_get_data(fullspectrum);
    realdata = gwy_data_line_get_data(rfft);
    imagdata = gwy_data_line_get_data(ifft);
    for (k = 0; k < n/2; k++)
        fsdata[k] = sqrt(realdata[k]*realdata[k] + imagdata[k]*imagdata[k]);

    calculate_weights(weights, fullspectrum, args);

    weightdata = gwy_data_line_get_data(weights);

    if (!gwy_app_wait_set_fraction(0.6))
        goto end;

    for (k = 0; k < n/2; k++) {
        realdata[k] *= weightdata[k];
        imagdata[k] *= weightdata[k];

        realdata[n-k-1] *= weightdata[k];
        imagdata[n-k-1] *= weightdata[k];
    }
    if (!gwy_app_wait_set_fraction(0.7))
        goto end;

    gwy_data_line_fft_raw(rfft, ifft, rresult, iresult, GWY_TRANSFORM_DIRECTION_BACKWARD);

    if (!gwy_app_wait_set_fraction(0.9))
        goto end;

    rresultdata = gwy_data_line_get_data(rresult);

    if (dir == DIR_X) {
       for (k = 0; k < n; k++) {
          xyz_result[k].x = rresultdata[k];
          xyz_result[k].y = xyz[k].y;
          xyz_result[k].z = xyz[k].z;
       }
    }
    else if (dir == DIR_Y) {
       for (k = 0; k < n; k++) {
          xyz_result[k].x = xyz[k].x;
          xyz_result[k].y = rresultdata[k];
          xyz_result[k].z = xyz[k].z;
       }
    }
    else {
       for (k = 0; k < n; k++) {
          xyz_result[k].x = xyz[k].x;
          xyz_result[k].y = xyz[k].y;
          xyz_result[k].z = rresultdata[k];
       }
    }

    ok = TRUE;

end:
    gwy_app_wait_finish();
    GWY_OBJECT_UNREF(rfft);
    GWY_OBJECT_UNREF(ifft);
    GWY_OBJECT_UNREF(rresult);
    GWY_OBJECT_UNREF(iresult);
    GWY_OBJECT_UNREF(weights);
    GWY_OBJECT_UNREF(fullspectrum);

    return ok;
}

static void
calculate_weights(GwyDataLine *weights, GwyDataLine *modulus, ModuleArgs *args)
{
    GwyFFTFilt1DSuppressType suppres = gwy_params_get_enum(args->params, PARAM_SUPPRESS);
    GwyFFTFilt1DOutputType output = gwy_params_get_enum(args->params, PARAM_OUTPUT);
    GwySelection *selection = args->graph_selection;
    gint res, k, nsel;
    gdouble sel[2], *w;
    gint fill_from, fill_to;

    res = gwy_data_line_get_res(weights);
    nsel = gwy_selection_get_data(selection, NULL);
    for (k = 0; k < nsel; k++) {
        gwy_selection_get_object(selection, k, sel);
        GWY_ORDER(gdouble, sel[0], sel[1]);
        fill_from = MAX(0, gwy_data_line_rtoi(weights, sel[0]));
        fill_from = MIN(res, fill_from);
        fill_to = MIN(res, gwy_data_line_rtoi(weights, sel[1]));
        gwy_data_line_part_fill(weights, fill_from, fill_to, 1.0);
    }

    w = gwy_data_line_get_data(weights);
    if (output == OUTPUT_MARKED) {
        for (k = 0; k < res; k++)
            w[k] = 1.0 - w[k];
    }

    /* For Suppress, interpolate PSDF linearly between endpoints.  Since we pass weights to the filter, not PSDF
     * itself, we have to divide to get the weight. */
    if (suppres == SUPPRESS_NEIGBOURHOOD) {
        GwyDataLine *buf = gwy_data_line_duplicate(modulus);
        gdouble *b, *m;

        /*invert the weights as the laplace correction treats it as a mask*/
        for (k = 0; k < res; k++)
            w[k] = 1.0 - w[k];

        gwy_data_line_correct_laplace(buf, weights);
        b = gwy_data_line_get_data(buf);
        m = gwy_data_line_get_data(modulus);

        for (k = 0; k < res; k++)
            w[k] = (m[k] > 0.0 ? fmin(b[k]/m[k], 1.0) : 0.0);
        g_object_unref(buf);
    }
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
