/* This is a GENERATED file. */
#include <glib.h>
#include <libgwymodule/gwymodule.h>

static const GwyModuleRecord* register_bundle(void);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION | GWY_MODULE_BUNDLE_FLAG,
    (GwyModuleRegisterFunc)&register_bundle,
    NULL, NULL, NULL, NULL, NULL,
};

GWY_MODULE_QUERY(module_info)

GwyModuleInfo* _gwy_module_query__xyzops(void);
GwyModuleInfo* _gwy_module_query__xyz_autocrop(void);
GwyModuleInfo* _gwy_module_query__xyz_crop(void);
GwyModuleInfo* _gwy_module_query__xyz_fft(void);
GwyModuleInfo* _gwy_module_query__xyz_level(void);
GwyModuleInfo* _gwy_module_query__xyz_raster(void);
GwyModuleInfo* _gwy_module_query__xyz_reduce(void);
GwyModuleInfo* _gwy_module_query__xyz_split(void);
GwyModuleInfo* _gwy_module_query__xyz_tcut(void);
GwyModuleInfo* _gwy_module_query__xyz_zdrift(void);

static const GwyModuleRecord modules[] = {
  { _gwy_module_query__xyzops, "xyzops", },
  { _gwy_module_query__xyz_autocrop, "xyz_autocrop", },
  { _gwy_module_query__xyz_crop, "xyz_crop", },
  { _gwy_module_query__xyz_fft, "xyz_fft", },
  { _gwy_module_query__xyz_level, "xyz_level", },
  { _gwy_module_query__xyz_raster, "xyz_raster", },
  { _gwy_module_query__xyz_reduce, "xyz_reduce", },
  { _gwy_module_query__xyz_split, "xyz_split", },
  { _gwy_module_query__xyz_tcut, "xyz_tcut", },
  { _gwy_module_query__xyz_zdrift, "xyz_zdrift", },
  { NULL, NULL, },
};

static const GwyModuleRecord*
register_bundle(void)
{
    return modules;
}
