/*
 *  $Id: xyz_reduce.c 26355 2024-05-21 08:23:55Z yeti-dn $
 *  Copyright (C) 2016-2023 David Necas (Yeti).
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public
 *  License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any
 *  later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 *  details.
 *
 *  You should have received a copy of the GNU General Public License along with this program; if not, write to the
 *  Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/surface.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-xyz.h>
#include <app/gwyapp.h>

#define RUN_MODES (GWY_RUN_INTERACTIVE | GWY_RUN_IMMEDIATE)

enum {
    PARAM_FACTOR,
    PARAM_METHOD,
};

typedef enum {
    XYZ_REDUCE_SKIP    = 0,
    XYZ_REDUCE_AVERAGE = 1,
} XYZReduceType;

typedef struct {
    GwyParams *params;
    GwySurface *surface;
    GwySurface *result;
} ModuleArgs;

static gboolean         module_register     (void);
static GwyParamDef*     define_module_params(void);
static void             execute             (ModuleArgs *args);
static GwyDialogOutcome run_gui             (ModuleArgs *args);
static void             xyzreduce           (GwyContainer *data,
                                             GwyRunType runtype);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("XYZ data size reduction by skipping points."),
    "Petr Klapetek <klapetek@gwyddion.net>",
    "1.0",
    "Petr Klapetek",
    "2024",
};

GWY_MODULE_QUERY2(module_info, xyz_reduce)

static gboolean
module_register(void)
{
    gwy_xyz_func_register("xyz_reduce",
                          (GwyXYZFunc)&xyzreduce,
                          N_("/_Reduce Number of Points..."),
                          NULL,
                          RUN_MODES,
                          GWY_MENU_FLAG_XYZ,
                          N_("Reduce XYZ data size by skipping points"));

    return TRUE;
}

static GwyParamDef*
define_module_params(void)
{
    static const GwyEnum methods[] = {
        { N_("Skip"),    XYZ_REDUCE_SKIP, },
        { N_("Average"), XYZ_REDUCE_AVERAGE, },
    };

    static GwyParamDef *paramdef = NULL;

    if (paramdef)
        return paramdef;

    paramdef = gwy_param_def_new();
    gwy_param_def_set_function_name(paramdef, gwy_xyz_func_current());
    gwy_param_def_add_int(paramdef, PARAM_FACTOR, "factor", _("Reduction factor"), 2, 1000, 10);
    gwy_param_def_add_gwyenum(paramdef, PARAM_METHOD, "method", _("Method"),
                              methods, G_N_ELEMENTS(methods), XYZ_REDUCE_SKIP);

    return paramdef;
}

static void
xyzreduce(GwyContainer *data, GwyRunType runtype)
{
    GwyDialogOutcome outcome = GWY_DIALOG_PROCEED;
    ModuleArgs args;
    gint id, newid;
    const guchar *gradient;

    g_return_if_fail(runtype & RUN_MODES);

    gwy_app_data_browser_get_current(GWY_APP_SURFACE, &args.surface,
                                     GWY_APP_SURFACE_ID, &id,
                                     0);
    g_return_if_fail(GWY_IS_SURFACE(args.surface));

    args.params = gwy_params_new_from_settings(define_module_params());
    if (runtype == GWY_RUN_INTERACTIVE) {
        outcome = run_gui(&args);
        gwy_params_save_to_settings(args.params);
        if (outcome == GWY_DIALOG_CANCEL)
            goto end;
    }

    if (outcome == GWY_DIALOG_PROCEED)
        execute(&args);

    newid = gwy_app_data_browser_add_surface(args.result, data, TRUE);
    gwy_app_set_surface_title(data, newid, _("Reduced points"));
    if (gwy_container_gis_string(data, gwy_app_get_surface_palette_key_for_id(id), &gradient))
        gwy_container_set_const_string(data, gwy_app_get_surface_palette_key_for_id(newid), gradient);

    g_object_unref(args.result);

end:
    g_object_unref(args.params);
}

static GwyDialogOutcome
run_gui(ModuleArgs *args)
{
    GwyDialog *dialog;
    GwyParamTable *table;

    dialog = GWY_DIALOG(gwy_dialog_new(_("Reduce XYZ Data")));
    gwy_dialog_add_buttons(dialog, GTK_RESPONSE_CANCEL, GTK_RESPONSE_OK, 0);

    table = gwy_param_table_new(args->params);

    gwy_param_table_append_radio(table, PARAM_METHOD);
    gwy_param_table_append_slider(table, PARAM_FACTOR);
    gwy_dialog_add_param_table(dialog, table);
    gwy_dialog_add_content(dialog, gwy_param_table_widget(table), FALSE, TRUE, 0);

    return gwy_dialog_run(dialog);
}

static void
execute(ModuleArgs *args)
{
    XYZReduceType method = gwy_params_get_enum(args->params, PARAM_METHOD);
    gint factor = gwy_params_get_int(args->params, PARAM_FACTOR);
    GwySurface *surface = args->surface;
    GwySurface *surf_result;
    const GwyXYZ *xyz;
    GwyXYZ *xyz_result;
    guint k, n, nresult, realnresult, nav;
    gint nk;
    gdouble xav, yav, zav;

    xyz = gwy_surface_get_data_const(surface);
    n = gwy_surface_get_npoints(surface);

    realnresult = 0;
    for (k = 0; k < n;  k += factor)
        realnresult++;

    if (realnresult < 3)
        realnresult = 3; //having too small nuber of points confuses preview window?
    args->result = surf_result = gwy_surface_new_sized(realnresult);
    xyz_result = gwy_surface_get_data(surf_result);
    gwy_surface_copy_units(surface, surf_result);

    nresult = 0;
    if (method == XYZ_REDUCE_SKIP) {
        for (k = 0; k < n; k += factor) {
            xyz_result[nresult].x = xyz[k].x;
            xyz_result[nresult].y = xyz[k].y;
            xyz_result[nresult].z = xyz[k].z;
            nresult++;
        }
    } else {
        xav = yav = zav = 0;
        nav = 0;
        for (k = 0; k < n; k++) {
            if (k%factor == 0 && nav > 0) {
                xyz_result[nresult].x = xav/nav;
                xyz_result[nresult].y = yav/nav;
                xyz_result[nresult].z = zav/nav;
                nresult++;

                xav = 0;
                yav = 0;
                zav = 0;
                nav = 0;
            }

            xav += xyz[k].x;
            yav += xyz[k].y;
            zav += xyz[k].z;
            nav++;
        }
    }
    if (nresult < realnresult) { //fill the rest if we had to increase number of points manually
        for (k = nresult; k < realnresult; k++) {
            nk = k - nresult;
            if (nk < 0)
                nk = 0;
            if (nk >= n)
                nk = n-1;

            xyz_result[k].x = xyz[nk].x;
            xyz_result[k].y = xyz[nk].y;
            xyz_result[k].z = xyz[nk].z;
        }
    }
}

/* vim: set cin columns=120 tw=118 et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
