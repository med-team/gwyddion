#!/usr/bin/python
# vim: set fileencoding=utf-8 :
# $Id: gen-authors.py 26451 2024-08-02 09:06:45Z yeti-dn $
from __future__ import division
import re, sys, locale
from xml.sax.saxutils import escape

if len(sys.argv) != 3 or sys.argv[2] not in ('web', 'header'):
    sys.stderr.write('gen-authors.py AUTHORS {web|header}\n')
    sys.exit(1)

filename = sys.argv[1]
mode = sys.argv[2]

cyrillic_translit = '''\
АAБBВVГGДDЕEЁEЖZЗZИIЙJКKЛLМMНNОOПPРRСSТTУUФFХHЦCЧCШSЩSЪZЫZЬZЭEЮUЯA\
аaбbвvгgдdеeёeжzзzиiйjкkлlмmнnоoпpрrсsтtуuфfхhцcчcшsщsъzыzьzэeюuяa\
'''
if sys.version_info.major == 2:
    cyrillic_translit = cyrillic_translit.decode('utf-8')

cyrillic_map = dict((cyrillic_translit[2*i], cyrillic_translit[2*i + 1])
                    for i in range(len(cyrillic_translit)//2))

contrib_re = re.compile(r'(?ms)^(?P<name>\S[^<>]+?)'
                        r'(\s+<(?P<email>[^<> ]+)>)?\n'
                        r'(?P<what>(?:^ +\S[^\n]+\n)+)')

def sortkey(x):
    last = [y for y in x.split() if not y.startswith('(')][-1]
    x = last + ' ' + x
    if sys.version_info.major == 2:
        x = ''.join(cyrillic_map.get(c, c) for c in x.decode('utf-8')).encode('utf-8')
    else:
        x = ''.join(cyrillic_map.get(c, c) for c in x)
    return locale.strxfrm(x)

def make_sectid(section):
    return re.sub(r'[^a-zA-Z]', '', section.lower())

def parse_contributors(text, section):
    header_re = re.compile(r'(?ms)^=== ' + section + r' ===(?P<body>.*?)^$')
    text = header_re.search(text).group('body')
    contributors = {}
    names = []
    for m in contrib_re.finditer(text):
        name, email = m.group('name'), m.group('email')
        what = re.sub(r'(?s)\s+', r' ', m.group('what').strip())
        contributors[name] = (email, what)
        names.append(name)
    return contributors, names

def format_html_list(text, section):
    contributors, names = parse_contributors(text, section)
    sectid = make_sectid(section)
    out = ['<p id="%s"><b>%s:</b></p>' % (sectid, section)]
    out.append('<dl>')
    for name in sorted(names, key=sortkey):
        email, what = contributors[name]
        out.append('<dt>%s, <code>%s</code></dt>' % (name, email))
        out.append('<dd>%s</dd>' % what)
    out.append('</dl>')
    return '\n'.join(out)

def format_header_array(text, section):
    contributors, names = parse_contributors(text, section)
    sectid = make_sectid(section)
    out = ['static const gchar %s[] =' % sectid]
    out.extend(['  "%s\\n"' % name for name in names])
    out.append(';')
    return '\n'.join(out)

text = open(filename).read()

if mode == 'web':
    # Can't set a UTF-8 locale on MS Windows so avoid it in the header mode.
    locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
    sys.stdout.write(format_html_list(text, 'Developers') + '\n')
    sys.stdout.write(format_html_list(text, 'Translators') + '\n')

if mode == 'header':
    sys.stdout.write('/* This is a %s file */\n' % 'GENERATED')
    sys.stdout.write(format_header_array(text, 'Developers') + '\n')
    sys.stdout.write(format_header_array(text, 'Translators') + '\n')
